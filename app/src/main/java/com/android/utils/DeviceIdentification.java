package com.android.utils;

import android.content.Context;

import com.android.gigasetsample.MainActivity;
import com.android.gigasetsample.R;

public class DeviceIdentification {

		public final int LENGTH = 15;
		public final int LENGTH_SECTION_1 = 4;
		public final int LENGTH_SECTION_2 = 6;
		public final int LENGTH_SECTION_3 = 5;
		public final int LENGTH_SECTIONS = 3;


		private Context mContext;

		public DeviceIdentification(){ mContext = MainActivity.instance;}

		/** Checking does the DID support or not. **/
		public boolean isSupported(String did) {
			if (!isValid(did)) {	return false;}

			for(String mDIDfilter : mContext.getResources().getStringArray(R.array.camera_did_sample_list)){
				if(did.subSequence(2,4).equals(mDIDfilter)){  // for xx
					return true;
				}else if(did.subSequence(0,4).equals(mDIDfilter)){  // for AHUA
					return true;
				}
			}

			if(mContext.getPackageName().equals("com.p2pcamera.app02hd")){
				for (String mDIDfilter : mContext.getResources().getStringArray(R.array.camera_did_black_list)) {
					if (!did.substring(2,4).equals(mDIDfilter)) {
						return true;
					}
				}

			}else {
				for (String mDIDfilter : mContext.getResources().getStringArray(R.array.camera_did_white_list)) {
					if (did.substring(2,4).equals(mDIDfilter)) {
						return true;
					}
				}
			}

			return false;
		}

		/** Checking does the DID valid or not. **/
		public boolean isValid(String did) {
			if (did == null) {	return false;}
			if (did.length() < LENGTH) {	return false;}
			String strFormatedDid = getFormatedDevId(did.getBytes());
			String[] strDidSec = strFormatedDid.split("-");
			if (strDidSec == null) {	return false;}
			if (strDidSec.length != LENGTH_SECTIONS) {	return false;}
			if (strDidSec[0].length() != LENGTH_SECTION_1) {	return false;}
			if (strDidSec[1].length() != LENGTH_SECTION_2) {	return false;}
			if (strDidSec[2].length() != LENGTH_SECTION_3) {	return false;}

			return true;
		}

		/** Formating DID, for saving in database. **/
		public String getFormatedDevId(byte[] dev_id){
			if(dev_id==null) return "";
			else{
				int iLen=0;
				StringBuffer sb=new StringBuffer("");
				boolean bIsCharacter = true;
				for(iLen=0; iLen<dev_id.length; iLen++){
					if(dev_id[iLen]==(byte)0) break;

					if(dev_id[iLen]>=0x30 && dev_id[iLen]<=0x39) { //0~9
						if(bIsCharacter){
							bIsCharacter = false;
							sb.append('-');
						}
						sb.append((char)dev_id[iLen]);

					}else if(dev_id[iLen]>=0x61 && dev_id[iLen]<=0x7A){ //'a'--'z'
						if(!bIsCharacter){
							bIsCharacter = true;
							sb.append('-');
						}
						sb.append((char)(dev_id[iLen]-0x61+0x41));

					}else if(dev_id[iLen]>=0x41 && dev_id[iLen]<=0x5A){ //'A'--'Z'
						if(!bIsCharacter){
							bIsCharacter = true;
							sb.append('-');
						}
						sb.append((char)dev_id[iLen]);

					}else if(dev_id[iLen] == '-');
					else break;
				}//for-end
				return sb.toString();
			}
		}
	
}
