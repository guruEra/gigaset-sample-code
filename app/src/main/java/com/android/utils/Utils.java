package com.android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.view.View;

import com.android.gigasetsample.R;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {

    public final static String MAGIC_KEY = "sg5BXQRD9xJD3Aqq-SJGE";

    public static boolean isConnectedToInternet(Context context) {
        if (context != null) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            return cm.getActiveNetworkInfo() != null ? cm
                    .getActiveNetworkInfo().isConnectedOrConnecting() : false;
        }
        return true;
    }
    public static void hideSystemUI(final Activity activity) {
       /* final View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);*/

        final View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // Hide navigation bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // Hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if (visibility == 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Automatically hide the system UI layout after 3 seconds if device is still in landscape mode
                            if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                                decorView.setSystemUiVisibility(
                                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // Hide nav bar
                                                | View.SYSTEM_UI_FLAG_FULLSCREEN // Hide status bar
                                                | View.SYSTEM_UI_FLAG_IMMERSIVE);
                            }
                        }
                    }, 3000);
                }
            }
        });
    }

    public static void showSystemUI(Activity activity) {
       /* View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);*/

        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(0);
        decorView.setOnSystemUiVisibilityChangeListener(null);
    }
    public static String genPwd(Context context,String mac) {
        final String chars = context.getResources().getString(R.string.pattern);
        char[] pwdFinal = {'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'};
//        byte pas[] = fromHexString("78 43 63 5a 71 70 6c 4b 37 78 44 66 53 7a 31 43 43 31 59 69 37 46 56 5a 70 76 32 78 65 70 4d 5a 59 47 35 79 6b 47 61 70 45 77 47 34 4f 6d 67 32 50 53 4e 58 6f 34 58 51 4b 79 78 36 54 46");
        try {

            int PWD_LEN = 14;
            int INTERNAL_MULTIPLIER = 12;
            byte[] pwdByte = SHA1(mac);
            for (int i = 0; i < PWD_LEN; ++i)
                pwdByte[i] = (byte) (pwdByte[i] + (pwdByte[PWD_LEN - i] * INTERNAL_MULTIPLIER));

            for (int i = 0; i < PWD_LEN; ++i) {
                int index = pwdByte[i];
                if (index < 0)
                    index = index & 0xff;
                pwdFinal[i] = chars.charAt(index % chars.length());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(pwdFinal);
    }

    public static byte[] SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        byte[] sha1hash = md.digest();
//        Log.d("TAG desf", "on onPause hash " + convertToHex(sha1hash));

        return sha1hash;
    }
}
