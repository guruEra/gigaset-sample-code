package com.android.services;



import com.android.models.GetSensorsModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET("service/camera/status")
    Call<GetSensorsModel> cameraStatus(@QueryMap Map<String, String> params);

}
