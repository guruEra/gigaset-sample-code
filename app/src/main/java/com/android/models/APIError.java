package com.android.models;

public class APIError {

    Error response;

    public Error getResponse() {
        return response;
    }

    public void setResponse(Error respone) {
        this.response = respone;
    }

    public class Error{

       private String status;
       private String description;
       public String status() {
           return status;
       }

       public String description() {
           return description;
       }
    }

    public APIError() {
    }


}
