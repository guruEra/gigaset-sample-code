package com.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class GetSensorsModel implements Parcelable {


    public static GetSensorsModel instance = null;

    public static GetSensorsModel getInstance() {
        if (instance == null) {
            instance = new GetSensorsModel();
        }
        return instance;
    }

    public static void setInstance(GetSensorsModel model) {
        instance = model;
    }

    /**
     * response : {"action":null,"result":null,"status":"SUCCESS","code":0,"description":"Sensors retrieved"}
     * data : [{"hubName":"My hub","sensorMap":{"SIREN":[{"sensorName":"ZZ1 sensor","mac":"SEN9008","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd454n.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false},{"sensorName":"ZZ2 sensor","mac":"SEN9007","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd454n.jpg","setupComplete":false,"petFriendly":false,"inHomeZone":false},{"sensorName":"ZZ3 sensor","mac":"SEN9006","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd454n.jpg","setupComplete":false,"petFriendly":false,"inHomeZone":false}],"CONTACT":[{"sensorName":"My sensor 99","mac":"SEN9005","status":"1","thumbnailPath":"s3:///vdfgjndfgjert/dfgdf9809t.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false}],"MOTION":[{"sensorName":"My sensor 1","mac":"SEN9002","status":"1","thumbnailPath":"s3:///vdfgjndfgjert/dfgdf9809t.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false},{"sensorName":"SENSOR_INITALLY_IN_HOME_ZONE","mac":"SENSOR_INITALLY_IN_HOME_ZONE","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd590o.jpg","setupComplete":true,"petFriendly":true,"inHomeZone":true}],"REMOTE":[{"sensorName":"My sensor 0","mac":"SEN9004","status":"0","thumbnailPath":"s3:///tweriotupweoirut/2345uhg.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false},{"sensorName":"My sensor 2","mac":"SEN9001","status":"1","thumbnailPath":"s3:///tweriotupweoirut/2345uhg.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false},{"sensorName":"My sensor 3","mac":"SEN9003","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd454n.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false}]}}]
     */


    private ResponseBean response;
    private List<DataBean> data;

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class ResponseBean implements Parcelable {
        /**
         * action : null
         * result : null
         * status : SUCCESS
         * code : 0
         * description : Sensors retrieved
         */

        private String action;
        private String result;
        private String status;
        private int code;
        private String description;

        public Object getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public Object getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.action);
            dest.writeString(this.result);
            dest.writeString(this.status);
            dest.writeInt(this.code);
            dest.writeString(this.description);
        }

        public ResponseBean() {
        }

        protected ResponseBean(Parcel in) {
            this.action = in.readString();
            this.result = in.readString();
            this.status = in.readString();
            this.code = in.readInt();
            this.description = in.readString();
        }

        public static final Creator<ResponseBean> CREATOR = new Creator<ResponseBean>() {
            @Override
            public ResponseBean createFromParcel(Parcel source) {
                return new ResponseBean(source);
            }

            @Override
            public ResponseBean[] newArray(int size) {
                return new ResponseBean[size];
            }
        };
    }

    public static class DataBean implements Parcelable {
        /**
         * hubName : My hubresult
         * sensorMap : {"SIREN":[{"sensorName":"ZZ1 sensor","mac":"SEN9008","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd454n.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false},{"sensorName":"ZZ2 sensor","mac":"SEN9007","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd454n.jpg","setupComplete":false,"petFriendly":false,"inHomeZone":false},{"sensorName":"ZZ3 sensor","mac":"SEN9006","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd454n.jpg","setupComplete":false,"petFriendly":false,"inHomeZone":false}],"CONTACT":[{"sensorName":"My sensor 99","mac":"SEN9005","status":"1","thumbnailPath":"s3:///vdfgjndfgjert/dfgdf9809t.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false}],"MOTION":[{"sensorName":"My sensor 1","mac":"SEN9002","status":"1","thumbnailPath":"s3:///vdfgjndfgjert/dfgdf9809t.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false},{"sensorName":"SENSOR_INITALLY_IN_HOME_ZONE","mac":"SENSOR_INITALLY_IN_HOME_ZONE","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd590o.jpg","setupComplete":true,"petFriendly":true,"inHomeZone":true}],"REMOTE":[{"sensorName":"My sensor 0","mac":"SEN9004","status":"0","thumbnailPath":"s3:///tweriotupweoirut/2345uhg.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false},{"sensorName":"My sensor 2","mac":"SEN9001","status":"1","thumbnailPath":"s3:///tweriotupweoirut/2345uhg.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false},{"sensorName":"My sensor 3","mac":"SEN9003","status":"1","thumbnailPath":"s3:///sdfuho34534i5d/dsfsd454n.jpg","setupComplete":true,"petFriendly":false,"inHomeZone":false}]}
         */
        private String mac;
        private long createdDate;
        private long updateDate;
        private String status;
        private String timezone;
        private String hardwareId;
        private String firmwareId;
        private String hubId;
        private String source;
        private String mode;
        private String connection;
        private String gsmStatus;
        private String interval;
        private String whatWillHubBeUsedFor;
        private String name;
        private String id;
        private String userId;
        private boolean subscriptionCancelled;
        private boolean tamper;
        private String plusButtonSetting;
        private String alarmNotificationType;
        private String hubName;
        private SensorMapBean sensorMap;
        private String forceArm;
        private boolean hotspotStatus;
        private boolean supportsHotspot;
        private boolean voucherValid;
        private boolean hotspotPasswordSet;
        private boolean firmwareUpgradeRequired;
        private String discountApplicable;
        private String voucherValidationMessage;
        List<PhoneNumbersBean> phoneNumbers;

        public List<PhoneNumbersBean> getPhoneNumbers() {
            return phoneNumbers;
        }

        public void setPhoneNumbers(List<PhoneNumbersBean> phoneNumbers) {
            this.phoneNumbers = phoneNumbers;
        }

        public boolean isHotspotPasswordSet() {
            return hotspotPasswordSet;
        }

        public boolean isSupportsHotspot() {
            return supportsHotspot;
        }

        public void setSupportsHotspot(boolean supportsHotspot) {
            this.supportsHotspot = supportsHotspot;
        }

        public void setHotspotPasswordSet(boolean hotspotPasswordSet) {
            this.hotspotPasswordSet = hotspotPasswordSet;
        }

        public static class PhoneNumbersBean {
            /**
             * id : 2578
             * phoneNumber : 7778057272
             * hub : 2111
             * createdDate : 1564759164000
             * countryCode : 44
             * phoneNumberType : PRIMARY
             * twilioE164Format : +447778057272
             * contactName : Gary
             */

            private int id;
            private String phoneNumber;
            private int hub;
            private long createdDate;
            private int countryCode;
            private String phoneNumberType;
            private String twilioE164Format;
            private String contactName;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public void setPhoneNumber(String phoneNumber) {
                this.phoneNumber = phoneNumber;
            }

            public int getHub() {
                return hub;
            }

            public void setHub(int hub) {
                this.hub = hub;
            }

            public long getCreatedDate() {
                return createdDate;
            }

            public void setCreatedDate(long createdDate) {
                this.createdDate = createdDate;
            }

            public int getCountryCode() {
                return countryCode;
            }

            public void setCountryCode(int countryCode) {
                this.countryCode = countryCode;
            }

            public String getPhoneNumberType() {
                return phoneNumberType;
            }

            public void setPhoneNumberType(String phoneNumberType) {
                this.phoneNumberType = phoneNumberType;
            }

            public String getTwilioE164Format() {
                return twilioE164Format;
            }

            public void setTwilioE164Format(String twilioE164Format) {
                this.twilioE164Format = twilioE164Format;
            }

            public String getContactName() {
                return contactName;
            }

            public void setContactName(String contactName) {
                this.contactName = contactName;
            }
        }
        public boolean isFirmwareUpgradeRequired() {
            return firmwareUpgradeRequired;
        }

        public void setFirmwareUpgradeRequired(boolean firmwareUpgradeRequired) {
            this.firmwareUpgradeRequired = firmwareUpgradeRequired;
        }

        public boolean isTamper() {
            return tamper;
        }

        public void setTamper(boolean tamper) {
            this.tamper = tamper;
        }

        public boolean isHotspotStatus() {
            return hotspotStatus;
        }

        public void setHotspotStatus(boolean hotspotStatus) {
            this.hotspotStatus = hotspotStatus;
        }

        public boolean getVoucherValid() {
            return voucherValid;
        }

        public void setVoucherValid(boolean voucherValid) {
            this.voucherValid = voucherValid;
        }

        public String getDiscountApplicable() {
            return discountApplicable;
        }

        public void setDiscountApplicable(String discountApplicable) {
            this.discountApplicable = discountApplicable;
        }

        public String getVoucherValidationMessage() {
            return voucherValidationMessage;
        }

        public void setVoucherValidationMessage(String voucherValidationMessage) {
            this.voucherValidationMessage = voucherValidationMessage;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getPlusButtonSetting() {
            return plusButtonSetting;
        }

        public void setPlusButtonSetting(String plusButtonSetting) {
            this.plusButtonSetting = plusButtonSetting;
        }

        public String getAlarmNotificationType() {
            return alarmNotificationType;
        }

        public void setAlarmNotificationType(String alarmNotificationType) {
            this.alarmNotificationType = alarmNotificationType;
        }

        public String getForceArm() {
            return forceArm;
        }

        public void setForceArm(String forceArm) {
            this.forceArm = forceArm;
        }

        public boolean isSubscriptionCancelled() {
            return subscriptionCancelled;
        }

        public void setSubscriptionCancelled(boolean subscriptionCancelled) {
            this.subscriptionCancelled = subscriptionCancelled;
        }

        public String getMac() {
            return mac;
        }

        public void setMac(String mac) {
            this.mac = mac;
        }

        public long getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(long createdDate) {
            this.createdDate = createdDate;
        }

        public long getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(long updateDate) {
            this.updateDate = updateDate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

        public String getHardwareId() {
            return hardwareId;
        }

        public void setHardwareId(String hardwareId) {
            this.hardwareId = hardwareId;
        }

        public String getFirmwareId() {
            return firmwareId;
        }

        public void setFirmwareId(String firmwareId) {
            this.firmwareId = firmwareId;
        }

        public String getHubId() {
            return hubId;
        }

        public void setHubId(String hubId) {
            this.hubId = hubId;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public String getConnection() {
            return connection;
        }

        public void setConnection(String connection) {
            this.connection = connection;
        }

        public String getGsmStatus() {
            return gsmStatus;
        }

        public void setGsmStatus(String gsmStatus) {
            this.gsmStatus = gsmStatus;
        }

        public String getInterval() {
            return interval;
        }

        public void setInterval(String interval) {
            this.interval = interval;
        }

        public String getWhatWillHubBeUsedFor() {
            return whatWillHubBeUsedFor;
        }

        public void setWhatWillHubBeUsedFor(String whatWillHubBeUsedFor) {
            this.whatWillHubBeUsedFor = whatWillHubBeUsedFor;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHubName() {
            return hubName;
        }

        public void setHubName(String hubName) {
            this.hubName = hubName;
        }

        public SensorMapBean getSensorMap() {
            return sensorMap;
        }

        public void setSensorMap(SensorMapBean sensorMap) {
            this.sensorMap = sensorMap;
        }

        public static class SensorMapBean implements Parcelable {
            private List<SIRENBean> SIREN = new ArrayList<>();
            private List<CONTACTBean> CONTACT = new ArrayList<>();
            private List<MOTIONBean> MOTION = new ArrayList<>();
            private List<REMOTEBean> REMOTE = new ArrayList<>();

            public List<SIRENBean> getSIREN() {
                return SIREN;
            }

            public void setSIREN(List<SIRENBean> SIREN) {
                this.SIREN = SIREN;
            }

            public List<CONTACTBean> getCONTACT() {
                return CONTACT;
            }

            public void setCONTACT(List<CONTACTBean> CONTACT) {
                this.CONTACT = CONTACT;
            }

            public List<MOTIONBean> getMOTION() {
                return MOTION;
            }

            public void setMOTION(List<MOTIONBean> MOTION) {
                this.MOTION = MOTION;
            }

            public List<REMOTEBean> getREMOTE() {
                return REMOTE;
            }

            public void setREMOTE(List<REMOTEBean> REMOTE) {
                this.REMOTE = REMOTE;
            }

            public static class SIRENBean implements Parcelable {
                /**
                 * sensorName : ZZ1 sensor
                 * mac : SEN9008
                 * status : 1
                 * thumbnailPath : s3:///sdfuho34534i5d/dsfsd454n.jpg
                 * setupComplete : true
                 * petFriendly : false
                 * inHomeZone : false
                 */

                private String sensorName;
                private String mac;
                private String status;
                private String thumbnailPath;
                private boolean setupComplete;
                private boolean petFriendly;
                private boolean inHomeZone;

                public String getSensorName() {
                    return sensorName;
                }

                public void setSensorName(String sensorName) {
                    this.sensorName = sensorName;
                }

                public String getMac() {
                    return mac;
                }

                public void setMac(String mac) {
                    this.mac = mac;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getThumbnailPath() {
                    return thumbnailPath;
                }

                public void setThumbnailPath(String thumbnailPath) {
                    this.thumbnailPath = thumbnailPath;
                }

                public boolean isSetupComplete() {
                    return setupComplete;
                }

                public void setSetupComplete(boolean setupComplete) {
                    this.setupComplete = setupComplete;
                }

                public boolean isPetFriendly() {
                    return petFriendly;
                }

                public void setPetFriendly(boolean petFriendly) {
                    this.petFriendly = petFriendly;
                }

                public boolean isInHomeZone() {
                    return inHomeZone;
                }

                public void setInHomeZone(boolean inHomeZone) {
                    this.inHomeZone = inHomeZone;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.sensorName);
                    dest.writeString(this.mac);
                    dest.writeString(this.status);
                    dest.writeString(this.thumbnailPath);
                    dest.writeByte(this.setupComplete ? (byte) 1 : (byte) 0);
                    dest.writeByte(this.petFriendly ? (byte) 1 : (byte) 0);
                    dest.writeByte(this.inHomeZone ? (byte) 1 : (byte) 0);
                }

                public SIRENBean() {
                }

                protected SIRENBean(Parcel in) {
                    this.sensorName = in.readString();
                    this.mac = in.readString();
                    this.status = in.readString();
                    this.thumbnailPath = in.readString();
                    this.setupComplete = in.readByte() != 0;
                    this.petFriendly = in.readByte() != 0;
                    this.inHomeZone = in.readByte() != 0;
                }

                public static final Creator<SIRENBean> CREATOR = new Creator<SIRENBean>() {
                    @Override
                    public SIRENBean createFromParcel(Parcel source) {
                        return new SIRENBean(source);
                    }

                    @Override
                    public SIRENBean[] newArray(int size) {
                        return new SIRENBean[size];
                    }
                };
            }

            public static class CONTACTBean implements Parcelable {
                /**
                 * sensorName : My sensor 99
                 * mac : SEN9005
                 * status : 1
                 * thumbnailPath : s3:///vdfgjndfgjert/dfgdf9809t.jpg
                 * setupComplete : true
                 * petFriendly : false
                 * inHomeZone : false
                 */

                private String sensorName;
                private String mac;
                private String status;
                private String thumbnailPath;
                private boolean setupComplete;
                private boolean petFriendly;
                private boolean inHomeZone;

                public String getSensorName() {
                    return sensorName;
                }

                public void setSensorName(String sensorName) {
                    this.sensorName = sensorName;
                }

                public String getMac() {
                    return mac;
                }

                public void setMac(String mac) {
                    this.mac = mac;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getThumbnailPath() {
                    return thumbnailPath;
                }

                public void setThumbnailPath(String thumbnailPath) {
                    this.thumbnailPath = thumbnailPath;
                }

                public boolean isSetupComplete() {
                    return setupComplete;
                }

                public void setSetupComplete(boolean setupComplete) {
                    this.setupComplete = setupComplete;
                }

                public boolean isPetFriendly() {
                    return petFriendly;
                }

                public void setPetFriendly(boolean petFriendly) {
                    this.petFriendly = petFriendly;
                }

                public boolean isInHomeZone() {
                    return inHomeZone;
                }

                public void setInHomeZone(boolean inHomeZone) {
                    this.inHomeZone = inHomeZone;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.sensorName);
                    dest.writeString(this.mac);
                    dest.writeString(this.status);
                    dest.writeString(this.thumbnailPath);
                    dest.writeByte(this.setupComplete ? (byte) 1 : (byte) 0);
                    dest.writeByte(this.petFriendly ? (byte) 1 : (byte) 0);
                    dest.writeByte(this.inHomeZone ? (byte) 1 : (byte) 0);
                }

                public CONTACTBean() {
                }

                protected CONTACTBean(Parcel in) {
                    this.sensorName = in.readString();
                    this.mac = in.readString();
                    this.status = in.readString();
                    this.thumbnailPath = in.readString();
                    this.setupComplete = in.readByte() != 0;
                    this.petFriendly = in.readByte() != 0;
                    this.inHomeZone = in.readByte() != 0;
                }

                public static final Creator<CONTACTBean> CREATOR = new Creator<CONTACTBean>() {
                    @Override
                    public CONTACTBean createFromParcel(Parcel source) {
                        return new CONTACTBean(source);
                    }

                    @Override
                    public CONTACTBean[] newArray(int size) {
                        return new CONTACTBean[size];
                    }
                };
            }

            public static class MOTIONBean implements Parcelable {
                /**
                 * sensorName : My sensor 1
                 * mac : SEN9002
                 * status : 1
                 * thumbnailPath : s3:///vdfgjndfgjert/dfgdf9809t.jpg
                 * setupComplete : true
                 * petFriendly : false
                 * inHomeZone : false
                 */

                private String sensorName;
                private String mac;
                private String status;
                private String thumbnailPath;
                private boolean setupComplete;
                private boolean petFriendly;
                private boolean inHomeZone;

                public String getSensorName() {
                    return sensorName;
                }

                public void setSensorName(String sensorName) {
                    this.sensorName = sensorName;
                }

                public String getMac() {
                    return mac;
                }

                public void setMac(String mac) {
                    this.mac = mac;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getThumbnailPath() {
                    return thumbnailPath;
                }

                public void setThumbnailPath(String thumbnailPath) {
                    this.thumbnailPath = thumbnailPath;
                }

                public boolean isSetupComplete() {
                    return setupComplete;
                }

                public void setSetupComplete(boolean setupComplete) {
                    this.setupComplete = setupComplete;
                }

                public boolean isPetFriendly() {
                    return petFriendly;
                }

                public void setPetFriendly(boolean petFriendly) {
                    this.petFriendly = petFriendly;
                }

                public boolean isInHomeZone() {
                    return inHomeZone;
                }

                public void setInHomeZone(boolean inHomeZone) {
                    this.inHomeZone = inHomeZone;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.sensorName);
                    dest.writeString(this.mac);
                    dest.writeString(this.status);
                    dest.writeString(this.thumbnailPath);
                    dest.writeByte(this.setupComplete ? (byte) 1 : (byte) 0);
                    dest.writeByte(this.petFriendly ? (byte) 1 : (byte) 0);
                    dest.writeByte(this.inHomeZone ? (byte) 1 : (byte) 0);
                }

                public MOTIONBean() {
                }

                protected MOTIONBean(Parcel in) {
                    this.sensorName = in.readString();
                    this.mac = in.readString();
                    this.status = in.readString();
                    this.thumbnailPath = in.readString();
                    this.setupComplete = in.readByte() != 0;
                    this.petFriendly = in.readByte() != 0;
                    this.inHomeZone = in.readByte() != 0;
                }

                public static final Creator<MOTIONBean> CREATOR = new Creator<MOTIONBean>() {
                    @Override
                    public MOTIONBean createFromParcel(Parcel source) {
                        return new MOTIONBean(source);
                    }

                    @Override
                    public MOTIONBean[] newArray(int size) {
                        return new MOTIONBean[size];
                    }
                };
            }

            public static class REMOTEBean implements Parcelable {
                /**
                 * sensorName : My sensor 0
                 * mac : SEN9004
                 * status : 0
                 * thumbnailPath : s3:///tweriotupweoirut/2345uhg.jpg
                 * setupComplete : true
                 * petFriendly : false
                 * inHomeZone : false
                 */

                private String sensorName;
                private String mac;
                private String status;
                private String thumbnailPath;
                private boolean setupComplete;
                private boolean petFriendly;
                private boolean inHomeZone;

                public String getSensorName() {
                    return sensorName;
                }

                public void setSensorName(String sensorName) {
                    this.sensorName = sensorName;
                }

                public String getMac() {
                    return mac;
                }

                public void setMac(String mac) {
                    this.mac = mac;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public String getThumbnailPath() {
                    return thumbnailPath;
                }

                public void setThumbnailPath(String thumbnailPath) {
                    this.thumbnailPath = thumbnailPath;
                }

                public boolean isSetupComplete() {
                    return setupComplete;
                }

                public void setSetupComplete(boolean setupComplete) {
                    this.setupComplete = setupComplete;
                }

                public boolean isPetFriendly() {
                    return petFriendly;
                }

                public void setPetFriendly(boolean petFriendly) {
                    this.petFriendly = petFriendly;
                }

                public boolean isInHomeZone() {
                    return inHomeZone;
                }

                public void setInHomeZone(boolean inHomeZone) {
                    this.inHomeZone = inHomeZone;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(this.sensorName);
                    dest.writeString(this.mac);
                    dest.writeString(this.status);
                    dest.writeString(this.thumbnailPath);
                    dest.writeByte(this.setupComplete ? (byte) 1 : (byte) 0);
                    dest.writeByte(this.petFriendly ? (byte) 1 : (byte) 0);
                    dest.writeByte(this.inHomeZone ? (byte) 1 : (byte) 0);
                }

                public REMOTEBean() {
                }

                protected REMOTEBean(Parcel in) {
                    this.sensorName = in.readString();
                    this.mac = in.readString();
                    this.status = in.readString();
                    this.thumbnailPath = in.readString();
                    this.setupComplete = in.readByte() != 0;
                    this.petFriendly = in.readByte() != 0;
                    this.inHomeZone = in.readByte() != 0;
                }

                public static final Creator<REMOTEBean> CREATOR = new Creator<REMOTEBean>() {
                    @Override
                    public REMOTEBean createFromParcel(Parcel source) {
                        return new REMOTEBean(source);
                    }

                    @Override
                    public REMOTEBean[] newArray(int size) {
                        return new REMOTEBean[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeList(this.SIREN);
                dest.writeList(this.CONTACT);
                dest.writeList(this.MOTION);
                dest.writeList(this.REMOTE);
            }

            public SensorMapBean() {
            }

            protected SensorMapBean(Parcel in) {
                this.SIREN = new ArrayList<SIRENBean>();
                in.readList(this.SIREN, SIRENBean.class.getClassLoader());
                this.CONTACT = new ArrayList<CONTACTBean>();
                in.readList(this.CONTACT, CONTACTBean.class.getClassLoader());
                this.MOTION = new ArrayList<MOTIONBean>();
                in.readList(this.MOTION, MOTIONBean.class.getClassLoader());
                this.REMOTE = new ArrayList<REMOTEBean>();
                in.readList(this.REMOTE, REMOTEBean.class.getClassLoader());
            }

            public static final Creator<SensorMapBean> CREATOR = new Creator<SensorMapBean>() {
                @Override
                public SensorMapBean createFromParcel(Parcel source) {
                    return new SensorMapBean(source);
                }

                @Override
                public SensorMapBean[] newArray(int size) {
                    return new SensorMapBean[size];
                }
            };
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.mac);
            dest.writeLong(this.createdDate);
            dest.writeLong(this.updateDate);
            dest.writeString(this.status);
            dest.writeString(this.timezone);
            dest.writeString(this.hardwareId);
            dest.writeString(this.firmwareId);
            dest.writeString(this.hubId);
            dest.writeString(this.source);
            dest.writeString(this.mode);
            dest.writeString(this.connection);
            dest.writeString(this.gsmStatus);
            dest.writeString(this.interval);
            dest.writeString(this.whatWillHubBeUsedFor);
            dest.writeString(this.name);
            dest.writeString(this.id);
            dest.writeString(this.userId);
            dest.writeByte(this.subscriptionCancelled ? (byte) 1 : (byte) 0);
            dest.writeByte(this.tamper ? (byte) 1 : (byte) 0);
            dest.writeString(this.plusButtonSetting);
            dest.writeString(this.alarmNotificationType);
            dest.writeString(this.hubName);
            dest.writeParcelable(this.sensorMap, flags);
            dest.writeString(this.forceArm);
            dest.writeByte(this.hotspotStatus ? (byte) 1 : (byte) 0);
            dest.writeByte(this.supportsHotspot ? (byte) 1 : (byte) 0);
            dest.writeByte(this.hotspotPasswordSet ? (byte) 1 : (byte) 0);
            dest.writeByte(this.voucherValid ? (byte) 1 : (byte) 0);
            dest.writeByte(this.firmwareUpgradeRequired ? (byte) 1 : (byte) 0);
            dest.writeString(this.discountApplicable);
            dest.writeString(this.voucherValidationMessage);
            dest.writeList(this.phoneNumbers);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.mac = in.readString();
            this.createdDate = in.readLong();
            this.updateDate = in.readLong();
            this.status = in.readString();
            this.timezone = in.readString();
            this.hardwareId = in.readString();
            this.firmwareId = in.readString();
            this.hubId = in.readString();
            this.source = in.readString();
            this.mode = in.readString();
            this.connection = in.readString();
            this.gsmStatus = in.readString();
            this.interval = in.readString();
            this.whatWillHubBeUsedFor = in.readString();
            this.name = in.readString();
            this.id = in.readString();
            this.userId = in.readString();
            this.subscriptionCancelled = in.readByte() != 0;
            this.tamper = in.readByte() != 0;
            this.plusButtonSetting = in.readString();
            this.alarmNotificationType = in.readString();
            this.hubName = in.readString();
            this.sensorMap = in.readParcelable(SensorMapBean.class.getClassLoader());
            this.forceArm = in.readString();
            this.hotspotStatus = in.readByte() != 0;
            this.supportsHotspot = in.readByte() != 0;
            this.hotspotPasswordSet = in.readByte() != 0;
            this.voucherValid = in.readByte() != 0;
            this.firmwareUpgradeRequired = in.readByte() != 0;
            this.discountApplicable = in.readString();
            this.voucherValidationMessage = in.readString();
            this.phoneNumbers = new ArrayList<PhoneNumbersBean>();
            in.readList(this.phoneNumbers, PhoneNumbersBean.class.getClassLoader());
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.response, flags);
        dest.writeList(this.data);
    }

    public GetSensorsModel() {
    }

    protected GetSensorsModel(Parcel in) {
        this.response = in.readParcelable(ResponseBean.class.getClassLoader());
        this.data = new ArrayList<DataBean>();
        in.readList(this.data, DataBean.class.getClassLoader());
    }

    public static final Creator<GetSensorsModel> CREATOR = new Creator<GetSensorsModel>() {
        @Override
        public GetSensorsModel createFromParcel(Parcel source) {
            return new GetSensorsModel(source);
        }

        @Override
        public GetSensorsModel[] newArray(int size) {
            return new GetSensorsModel[size];
        }
    };


}
