package com.android.gigasetsample;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.webkit.WebView;


import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.android.services.RetrofitClient;
import com.jsw.sdk.p2p.device.P2PDev;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Shayne on 20/01/16.
 */
public class EvoApplication extends MultiDexApplication {

    public boolean isInBackground = false;
        private static EvoApplication instance;
    public static final String KEY_FUNC_GCM_NOTIFICATION_SWITCH = "key_gcm_notification_switch";
    public static final String KEY_FUNC_LIVEVIEW_HW_DECODE_SWITCH = "key_liveview_hw_decode_switch";
    public static final String NAME_APP_PREFERENCE = "omg_preference";
    public static final String VALUE_FUNC_ON = "value_on";
    public static final String VALUE_FUNC_OFF = "value_off";
    private boolean DEBUG = true;
    private String TAG = EvoApplication.class.getSimpleName();
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        P2PDev.initAll(getString(R.string.aws_id));
        RetrofitClient.setMcon(getApplicationContext());
        instance = this;

        String customFontFileNameInAssets = "fonts/proximanova_regular.ttf";
        String defaultFontNameToOverride = "SERIF";
        try {
            final Typeface customFontTypeface = Typeface.createFromAsset(getApplicationContext().getAssets(), customFontFileNameInAssets);
            replaceFont(defaultFontNameToOverride, customFontTypeface);

            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);


            enableWebViewDebug();
        } catch (Exception e) {
            Log.e("TAG", "Can not set custom font " + customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);
        }


        /* Checking does the GCM notification saved or not (default is ON). */
        if (getPreference(KEY_FUNC_GCM_NOTIFICATION_SWITCH) == null) {
            getSharedPreferences(NAME_APP_PREFERENCE, Context.MODE_PRIVATE).edit()
                    .putString(KEY_FUNC_GCM_NOTIFICATION_SWITCH, VALUE_FUNC_ON)
                    .commit();
            if (DEBUG) {
                Log.d(TAG, "onCreate(): KEY_FUNC_GCM_NOTIFICATION_SWITCH=" + getPreference(KEY_FUNC_GCM_NOTIFICATION_SWITCH));
            }
        }

        /* Checking does the switch of LiveView HW decoding saved or not (default is ON). */
        if (getPreference(KEY_FUNC_LIVEVIEW_HW_DECODE_SWITCH) == null) {
            getSharedPreferences(NAME_APP_PREFERENCE, Context.MODE_PRIVATE).edit()
                    .putString(KEY_FUNC_LIVEVIEW_HW_DECODE_SWITCH, VALUE_FUNC_OFF)
                    .commit();
            if (DEBUG) {
                Log.d(TAG, "onCreate(): KEY_FUNC_LIVEVIEW_HW_DECODE_SWITCH=" + getPreference(KEY_FUNC_LIVEVIEW_HW_DECODE_SWITCH));
            }
        }
    }
    public static EvoApplication getInstance() {
        return instance;
    }

    public static boolean hasNetwork() {
        return instance.checkIfHasNetwork();
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
    protected static void replaceFont(String staticTypefaceFieldName, final Typeface newTypeface) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//isVersionGreaterOrEqualToLollipop()) {
            Map<String, Typeface> newMap = new HashMap<String, Typeface>();
            newMap.put("sans-serif", newTypeface);
            try {
                final Field staticField = Typeface.class.getDeclaredField("sSystemFontMap");
                staticField.setAccessible(true);
                staticField.set(null, newMap);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                final Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);
                staticField.setAccessible(true);
                staticField.set(null, newTypeface);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
    public String getPreference(String key) {
        return getSharedPreferences(NAME_APP_PREFERENCE, Context.MODE_PRIVATE).getString(key, null);
    }


    protected void enableWebViewDebug(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE))
            { WebView.setWebContentsDebuggingEnabled(true); }
        }
    }
}
