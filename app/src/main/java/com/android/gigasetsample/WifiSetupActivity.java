package com.android.gigasetsample;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jsw.sdk.activity.helper.SettingAdvancedHelper;
import com.jsw.sdk.general.Packet;
import com.jsw.sdk.general.TimerRefresh;
import com.jsw.sdk.p2p.device.IAVListener;
import com.jsw.sdk.p2p.device.IRecvIOCtrlListener;
import com.jsw.sdk.p2p.device.P2PDev;
import com.jsw.sdk.p2p.device.extend.AUTH_AV_IO_Proto;
import com.jsw.sdk.p2p.device.extend.DebugName;
import com.jsw.sdk.p2p.device.extend.EX_IOCTRL_APNS_SERVER_PUSH;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLDoorChimeRingtone;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLDoorbellSpeakerVolume;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLEmailSetting;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLExtensionDoorbellFunctions;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLGetMotionDetectResp;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLSetWifiReq;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLWifiExternSetting;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRL_ArmSetting;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRL_LIGHTING;
import com.jsw.sdk.p2p.device.extend.Ex_SWifiAp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import static com.jsw.sdk.p2p.device.extend.DebugName.Debug;

public class WifiSetupActivity extends AppCompatActivity implements IAVListener, IRecvIOCtrlListener, TimerRefresh.IUpdate {

    TextView txtSeconds;
    LinearLayout rlCount;
    TextView txtWiFiSSID;
    TextView txtWiFiStatus,txtDescription;
    RecyclerView recycleView;
    ProgressBar progressBar;
    LinearLayout container;
    public static P2PDev m_curCamera;
    private Ex_IOCTRLWifiExternSetting mWifiExternSetting;
    private SettingAdvancedHelper mAdvSettingHelper = null;
    private ArrayList<Ex_SWifiAp> m_wifiList = new ArrayList<Ex_SWifiAp>();
    private WifiAdapter adapter;
    private CountDownTimer countDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_setup);
        txtSeconds = findViewById(R.id.txtSeconds);
        rlCount = findViewById(R.id.rlCount);
        txtWiFiStatus = findViewById(R.id.txt_status);
        txtDescription = findViewById(R.id.txtDescription);
        txtWiFiSSID = findViewById(R.id.txt_ssid);
        recycleView = findViewById(R.id.recycleView);
        progressBar = findViewById(R.id.proressBar);
        container = findViewById(R.id.container);
        setTitle("Wifi Config");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recycleView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new WifiAdapter();
        recycleView.setAdapter(adapter);
        m_curCamera = MainActivity.m_curCamera;
        if (m_curCamera != null) {
            m_curCamera.regRecvIOCtrlListener(this);
            m_curCamera.regAVListener(this);
        }

        mAdvSettingHelper = new SettingAdvancedHelper(m_curCamera);
        mWifiExternSetting = new Ex_IOCTRLWifiExternSetting(m_curCamera);
        if (m_curCamera.mParam.isSupportWifiController()) {
            initWifiController();
        }
        if (m_curCamera.mParam.isSupportWifiSetting()) {

            getWiFiStatusFromRemote(0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDown != null) {
            countDown.cancel();
        }
    }

    private void initWifiController() {
        mWifiExternSetting.fetchWifiController();
    }
    private void getWiFiStatusFromRemote(int flag) {
        if (m_curCamera != null) {
            byte[] req = new byte[8];
            Arrays.fill(req, (byte) 0);
            req[0] = (byte) flag; // =0 list all wifi around device; =1 get
            // status of current connnected wifi
            m_curCamera.sendIOCtrl_outer(AUTH_AV_IO_Proto.IOCTRL_TYPE_LISTWIFIAP_REQ, req, req.length);
            System.out.println(" getWiFiStatusFromRemote");
        }
    }

    @Override
    public void onTimerUpdate() {
        // System.out.println("ActivitySettingAdvanced, onTimerUpdate");
        // getDeviceInfoFromRemoteSite();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateVFrame(Bitmap bmp, Object o) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateAVInfo(int codeInfo, Object o, int errCode, int intValue) {
        Message msg = handler.obtainMessage();
        msg.what = codeInfo;
        msg.arg1 = errCode;
        msg.arg2 = intValue;
        msg.obj = o;
        handler.sendMessage(msg);
    }

    @Override
    public void updateAVInfo2(int codeInfo, Object o, int errCode, int intValue) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onRecvIOCtrlData(int codeInfo, Object o, byte[] data) {
        Message msg = handler.obtainMessage();
        msg.what = codeInfo;
        msg.obj = o;

        if (data != null) {
            Bundle bundle = new Bundle();
            bundle.putByteArray("data", data);
            msg.setData(bundle);
        }
        handler.sendMessage(msg);
    }
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            byte[] data = null;
            if (bundle != null) {
                data = bundle.getByteArray("data");
            }
            Log.e("camera wifi", "code =" + msg.what);

            switch (msg.what) {
                case P2PDev.CONN_INFO_CONNECTING:
                    Log.e("camera wifi", "CONN_INFO_CONNECTING code =" + msg.what);

                    break;
                case P2PDev.CONN_INFO_CONNECTED:
                    Log.e("camera wifi", "CONN_INFO_CONNECTED code =" + msg.what);

                    break;
                case P2PDev.CONN_INFO_CONNECT_FAIL:

                    break;
                case P2PDev.CONN_INFO_SESSION_CLOSED: {
                    Debug(DebugName.EXTERNAL_WIFI_ON_OFF, "CONN_INFO_SESSION_CLOSED", "ActivityAdvancedSetting");

                }
                break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_DEVICE_CUSTOM_INFO_RESP: {// Tiger

                    break;
                }
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_SETPASSWORD_RESP: {


                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_SET_ADMIN_PASSWORD_RESP: {

                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_VIDEO_PARAMETER_RESP: {

                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_VIDEO_BRIGHTNESS_RESP: {


                }
                break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_LISTWIFIAP_RESP: {
                    int cnt = Packet.byteArrayToInt_Little(data, 0);
                    int size = Ex_SWifiAp.getTotalSize();
                    byte[] ssid = null;
                        progressBar.setVisibility(View.GONE);
                    boolean isWifiConnected = false;
                    m_wifiList.clear();
                    System.out.println("IOCTRL_TYPE_LISTWIFIAP_RESP, count=" + cnt
                            + ", data.length=" + data.length);
                    if (cnt > 0 && data.length >= 40) {
                        int pos = 4;
                        for (int i = 0; i < cnt; i++) {
                            ssid = new byte[32];
                            Arrays.fill(ssid, (byte) 0);
                            System.arraycopy(data, i * size + pos, ssid, 0, 31);

                            byte mode = data[i * size + pos + 32];
                            byte enctype = data[i * size + pos + 33];
                            byte signal = data[i * size + pos + 34];
                            byte status = data[i * size + pos + 35];

                            m_wifiList.add(new Ex_SWifiAp(ssid, mode, enctype,
                                    signal, status));
                            adapter.notifyDataSetChanged();
                            System.out.println("ssid=" +  new String(ssid)
                                    + ", status=" + status);
                            if (status == 1) {
                                isWifiConnected = true;
                                txtWiFiSSID.setText(new String(ssid));
                                txtWiFiSSID.setTypeface(null, Typeface.BOLD);
                                txtWiFiStatus
                                        .setText(getText(R.string.tips_wifi_connected));
//							bWIFIConnected = true;
                            } else if (status == 2) {
                                isWifiConnected = true;
                                txtWiFiSSID.setText(getString(0, ssid));
                                txtWiFiSSID.setTypeface(null, Typeface.BOLD);
                                txtWiFiStatus
                                        .setText(getText(R.string.tips_wifi_wrongpassword));

                            } else if (status == 3) {
                                isWifiConnected = true;
                                txtWiFiSSID.setText(new String(ssid));
                                txtWiFiSSID.setTypeface(null, Typeface.BOLD);
                                txtWiFiStatus
                                        .setText(getText(R.string.tips_wifi_weak_signal));

                            } else if (status == 4) {
                                isWifiConnected = true;
                                txtWiFiSSID.setText(new String(ssid));
                                txtWiFiSSID.setTypeface(null, Typeface.BOLD);
                                txtWiFiStatus
                                        .setText(getText(R.string.tips_wifi_connecting));

                            } else if (status == 5) {
                                isWifiConnected = true;
                                txtWiFiSSID.setText(new String(ssid));
                                txtWiFiSSID.setTypeface(null, Typeface.BOLD);
                                txtWiFiStatus
                                        .setText(getText(R.string.tips_wifi_invalid_ssid));
                            }
                        }
                    }
                    if (!isWifiConnected) {
                        txtWiFiSSID.setText(getText(R.string.tips_wifi_none));
                        txtWiFiSSID.setTypeface(null, Typeface.BOLD);
                        txtWiFiStatus.setText("");
                    }
                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_SETWIFI_RESP: {
                    if (data == null) {
                        break;
                    }
                    setWifiSettingMessage(data);

                }
                break;

                case Ex_IOCTRLGetMotionDetectResp.GETDETECTMODE_RESP: {

                    break;
                }
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_MOTION_SENSITIVITY_RESP: {

                }
                break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GETMOTIONDETECT_RESP: {

                }
                break;

                case Ex_IOCTRLEmailSetting.GET_EMAIL_RESP: {

//				reqEmailFinish = true;
//				checkAllDataIsLoad();
                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_FORMATEXTSTORAGE_RESP: {

                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_UPGRADE_FIRMWARE_RESP: {  //wapp fw update command responds

                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_SEND_FIRMWARE_IMAGE_STATUS_RESP:

                    break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_DEVINFO_RESP:

                    break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_TIMEZONE_RESP: {

//				checkAllDataIsLoad();
                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_ON_OFF_VALUE_RESP: {
                }
                break;

                case EX_IOCTRL_APNS_SERVER_PUSH.GET_APNS_SERVER_PUSH_RESP: {

                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_MOTION_MASK_RESP: {
                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_TIMESTAMP_SWITCH_RESP: {
                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_RVDP_SWITCH_STATUS_RESP: {


                }
                break;
                case Ex_IOCTRL_LIGHTING.GET_LIGHTING_SETTING_RESP:


                    break;
                case Ex_IOCTRL_ArmSetting.GET_ARM_RESP:

                    break;

                case Ex_IOCTRL_ArmSetting.SET_ARM_RESP:
                    break;

                case Ex_IOCTRLDoorbellSpeakerVolume.GET_AUDIO_SETTING_RESP:

                    break;
                case Ex_IOCTRLExtensionDoorbellFunctions.GET_RVDP_EXTERN_RELAY_RESP:
                    break;
                case Ex_IOCTRLWifiExternSetting.GETWIFI_EXTERN_RESP:
                    mWifiExternSetting.setData(data);

                    break;
                case Ex_IOCTRLWifiExternSetting.SETWIFI_EXTERN_RESP:

                    Debug(DebugName.EXTERNAL_WIFI_ON_OFF, "IOCTRL_TYPE_SETWIFI_EXTERN_RESP", "ActivityAdvancedSetting", "REBOOT");
                    mAdvSettingHelper.rebootSystem();
                    new CountDownTimer(2000, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            finish();

                        }

                    }.start();

                    break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_CLOUD_LIMIT_RESP:
                    Log.v("", "IOCTRL_TYPE_GET_CLOUD_LIMIT_RESP");

                    break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_CLOUD_PARM_RESP:

                    break;
                case Ex_IOCTRLDoorChimeRingtone.GET_RVDP_DOOR_BELL_RESP: {  //same as Ex_IOCTRL_MelodySetting.GET_RVDP_DOOR_BELL_RESP

                }
                break;
                case Ex_IOCTRLDoorChimeRingtone.SET_RVDP_DOOR_BELL_RESP: {

                }
                break;

                default:
                    ;
            }
        }
    };
    private void setWifiSettingMessage(byte[] data) {
        int result = (int) (data[0] & 0xFF);
        int dlgMsg = R.string.wifi_setting_error;
        int countdown_2 = -1;
        if (result == -1) {
            dlgMsg = R.string.wifi_setting_error;
        } else if (result == 0) {  //success with LAN connection
            dlgMsg = R.string.Wifi_SettingWarn_no_need_unplug;
            countdown_2 = 100;
        } else if (result == 1) {  //success with AP mode
            dlgMsg = R.string.Wifi_SettingWarn_no_need_unplug;
            countdown_2 = 60;
        } else if (result == 2) {  //success with STA mode
            dlgMsg = R.string.Wifi_SettingWarn_no_need_unplug;
            countdown_2 = 60;
        }
        rlCount.setVisibility(View.VISIBLE);
        txtDescription.setText(dlgMsg);
        final int countdown_in_main = countdown_2 + 20;
        countDown = new CountDownTimer(countdown_in_main * 1000,1000){
            @Override
            public void onTick(long l) {

                long sec = l / 1000;
                txtSeconds.setText("" + sec);
            }

            @Override
            public void onFinish() {
                onBackPressed();
            }
        };
        countDown.start();
    }
    class WifiAdapter extends RecyclerView.Adapter<WifiAdapter.myHolder> {

        AlertDialog dialog;

        public WifiAdapter() {

        }

        @NonNull
        @Override
        public myHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = getLayoutInflater().inflate(R.layout.wifi_list_item, null);
            return new myHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull myHolder myHolder, final int pos) {
            myHolder.txtSsid.setText(new String(m_wifiList.get(pos).ssid).trim());

            myHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(WifiSetupActivity.this, R.style.Theme_AppCompat_DayNight_Dialog);
                    final View view = View.inflate(WifiSetupActivity.this, R.layout.manage_device_wifi, null);
                    builder.setView(view);

                    final TextView txtWiFiSignal = (TextView) view.findViewById(R.id.txtWiFiSignal);
                    final TextView txtWiFiName = (TextView) view.findViewById(R.id.txtWiFiName);
                    final TextView txtWiFiSecurity = (TextView) view.findViewById(R.id.txtWiFiSecurity);
                    final EditText edtWiFiPassword = (EditText) view.findViewById(R.id.edtWiFiPassword);
                    final CheckBox checkBox = (CheckBox) view.findViewById(R.id.chbShowHiddenPassword);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            hideKeyboard(view, WifiSetupActivity.this);
                            String pwd = edtWiFiPassword.getText().toString();
                            String str0 = pwd, str1 = "", str2 = "";
                            char ch_temp0 = '"';
                            String strp = String.valueOf(ch_temp0);
                            int i_pwd_length = pwd.length();
                            for (int i = 0; i < i_pwd_length; i++) {
                                str1 = str0.substring(0 + i, 1 + i);
                                if (str1.equals("<")) {
                                    str1 = "&lt;";
                                } else if (str1.equals(">")) {
                                    str1 = "&gt;";
                                } else if (str1.equals("&")) {
                                    str1 = "&amp;";
                                } else if (str1.equals("'")) {
                                    str1 = "&apos;";
                                } else if (str1.equals(strp)) {
                                    str1 = "&quot;";
                                }
                                str2 = str2 + str1;
                            }
                            pwd = str2;

                            Ex_SWifiAp wifi = m_wifiList.get(pos);
                            if (m_curCamera != null && wifi != null && m_curCamera.getConnInfo() >= P2PDev.CONN_INFO_CONNECTED) {
                                txtWiFiSSID.setText(new String(wifi.ssid).trim());
                                txtWiFiSSID.setTypeface(null, Typeface.BOLD);
                                txtWiFiStatus.setText(getText(R.string.tips_wifi_connecting));
                                byte[] req = Ex_IOCTRLSetWifiReq.toBytes(wifi.ssid, wifi.mode, wifi.enctype, pwd.getBytes());
                                int nRet = m_curCamera.sendIOCtrl_outer(AUTH_AV_IO_Proto.IOCTRL_TYPE_SETWIFI_REQ, req, req.length);

                                if (nRet < 0) {
                                    Toast.makeText(WifiSetupActivity.this,
                                            "Send command failure, error " + nRet, Toast.LENGTH_SHORT).show();
                                } else {
                                }

                            } else {
                                Toast.makeText(WifiSetupActivity.this, "fail...", Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog = builder.create();
//                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCanceledOnTouchOutside(false);
                    Ex_SWifiAp wifi = m_wifiList.get(pos);
                    String security = "";
                    if (wifi.enctype == AUTH_AV_IO_Proto.IOCTRL_WIFIAPENC_INVALID)
                        security = "Invalid";
                    else if (wifi.enctype == AUTH_AV_IO_Proto.IOCTRL_WIFIAPENC_NONE)
                        security = "None";
                    else if (wifi.enctype == AUTH_AV_IO_Proto.IOCTRL_WIFIAPENC_WEP)
                        security = "WEP";
                    else if (wifi.enctype == AUTH_AV_IO_Proto.IOCTRL_WIFIAPENC_WPA2_AES)
                        security = "WPA2 AES";
                    else if (wifi.enctype == AUTH_AV_IO_Proto.IOCTRL_WIFIAPENC_WPA2_TKIP)
                        security = "WPA2 TKIP";
                    else if (wifi.enctype == AUTH_AV_IO_Proto.IOCTRL_WIFIAPENC_WPA_AES)
                        security = "WPA AES";
                    else if (wifi.enctype == AUTH_AV_IO_Proto.IOCTRL_WIFIAPENC_WPA_TKIP)
                        security = "WPA TKIP";
                    else
                        security = getText(R.string.Unknown).toString();

                    txtWiFiName.setText(m_wifiList.get(pos).getStringOfSSID(0));
                    txtWiFiSecurity.setText(security);
                    txtWiFiSignal.setText((int) wifi.signal + " %");

                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            if (!isChecked)
                                edtWiFiPassword
                                        .setTransformationMethod(PasswordTransformationMethod
                                                .getInstance());
                            else
                                edtWiFiPassword
                                        .setTransformationMethod(HideReturnsTransformationMethod
                                                .getInstance());
                        }
                    });

                    dialog.show();
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }
            });
        }

        @Override
        public int getItemCount() {
            return m_wifiList.size();
        }

        class myHolder extends RecyclerView.ViewHolder {
 
            TextView txtSsid;

            public myHolder(@NonNull View itemView) {
                super(itemView);
                txtSsid = itemView.findViewById(R.id.txt_ssid); 
            }
        }
    }
    public static void hideKeyboard(View view, Activity mContext) {
        // Check if no views has focus:
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//        View view = mContext.getCurrentFocus();
       /* mContext.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);*/
//        View view = mContext.getWindow().getDecorView();
//        if (view != null) {
//            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I do not want this...
                // Home as up button is to navigate to Home-Activity not previous acitivity
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
