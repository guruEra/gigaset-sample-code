package com.android.gigasetsample;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.SwitchCompat;

import com.AVS360.Avisonic.player.SphericalVideoPlayer;
import com.android.models.APIError;
import com.android.models.ErrorUtils;
import com.android.models.GetSensorsModel;
import com.android.services.RetrofitClient;
import com.android.utils.DeviceIdentification;
import com.android.utils.Utils;
import com.google.android.material.snackbar.Snackbar;
import com.jsw.sdk.activity.helper.LiveViewHelper;
import com.jsw.sdk.activity.helper.SettingAdvancedHelper;
import com.jsw.sdk.decoder.HwH264Decoder;
import com.jsw.sdk.general.DisplayInformation;
import com.jsw.sdk.general.NetworkInformation;
import com.jsw.sdk.general.TimerRefresh;
import com.jsw.sdk.p2p.device.IAVListener;
import com.jsw.sdk.p2p.device.IRecvIOCtrlListener;
import com.jsw.sdk.p2p.device.LanSearchManager;
import com.jsw.sdk.p2p.device.P2PDev;
import com.jsw.sdk.p2p.device.extend.AUTH_AV_IO_Proto;
import com.jsw.sdk.p2p.device.extend.DebugName;
import com.jsw.sdk.p2p.device.extend.EX_IOCTRL_PTAction;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLAVStream;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLGetMotionDetectResp;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLGetOnetDevInfoResp;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLGetVideoParameterResp;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLLightSwitch;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRLSensorSetup;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRL_ArmSetting;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRL_InstantAlarm;
import com.jsw.sdk.p2p.device.extend.Ex_IOCTRL_LIGHTING;
import com.jsw.sdk.p2p.device.extend.SearchLAN_Result;
import com.jsw.sdk.p2p.device.extend.SensorInfo;
import com.jsw.sdk.p2p.device.model.ModelHelperSDK;
import com.jsw.sdk.ui.TouchedTextureView;
import com.jsw.sdk.ui.TouchedView;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.jsw.sdk.p2p.device.extend.DebugName.Debug;

public class MainActivity extends AppCompatActivity implements IAVListener,
        TimerRefresh.IUpdate, IRecvIOCtrlListener, View.OnClickListener {
// ini
    private static final int RC_CAMERA_AND_LOCATION = 122;
    LinearLayout linoutViewArea;
    RelativeLayout view_re_layout_parent;
    RelativeLayout rlContainer;
    TouchedView m_viewLive;
    TouchedTextureView m_viewLiveHw;
    EditText etMac, etMacNew, etDid;
    AppCompatButton btConnect, btAddNew, btAddNewCon;
    AppCompatButton btnSnap, btCancel;
    AppCompatButton btnIntercom;
    SwitchCompat switchArm, switchCam, switchAudio;
    TextView txvConnectStatus;
    TextView tvArm, txtQual;
    LinearLayout llLight, llTimezone, llVideoQuality, llWifi, llAddNew, llSetting,llBtn;
    TextView tvLightStatus;
    SphericalVideoPlayer videoPlayer;
    ProgressDialog pd;
    public static P2PDev m_curCamera = null;
    public static MainActivity instance;
    private boolean isIntercom = false;
    private int sdcardStatus = 0;
    String macId;
    private boolean isAvailable = false;
    private static final String TAG = "MainActivity";
    private static final boolean DEBUG = true;
    private static final boolean DEBUG_HANDLER = true;
    private DeviceIdentification mDeviceIdentification = null;
    final int IOCTRL_TYPE_SETDETECTMODE_REQ = 65,
            IOCTRL_TYPE_SETDETECTMODE_RESP = 66,
            IOCTRL_TYPE_GETDETECTMODE_REQ = 67,
            IOCTRL_TYPE_GETDETECTMODE_RESP = 68;
    final int off = 0, pir = 1;
    private int lightStatusSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linoutViewArea = findViewById(R.id.linoutViewArea);
        view_re_layout_parent = findViewById(R.id.view_re_layout_parent);
        rlContainer = findViewById(R.id.rlContainer);
        m_viewLive = findViewById(R.id.tochvLiveView);
        m_viewLiveHw = findViewById(R.id.tochvHwDecodeLiveView);
        etMac = findViewById(R.id.etMac);
        etMacNew = findViewById(R.id.etMacNew);
        etDid = findViewById(R.id.etDid);
        btConnect = findViewById(R.id.btConnect);
        btAddNew = findViewById(R.id.btAddNew);
        btAddNewCon = findViewById(R.id.btAddNewCon);
        llBtn = findViewById(R.id.llBtn);
        btCancel = findViewById(R.id.btCancel);
        btnIntercom = findViewById(R.id.btnIntercom);
        btnSnap = findViewById(R.id.btnSnap);
        txtQual = findViewById(R.id.tvVideo);
        txvConnectStatus = findViewById(R.id.tvStatus);
        switchArm = findViewById(R.id.switchArm);
        switchCam = findViewById(R.id.switchCam);
        llWifi = findViewById(R.id.llWifi);
        switchAudio = findViewById(R.id.switchAudio);
        llVideoQuality = findViewById(R.id.llVideoQuality);
        llTimezone = findViewById(R.id.llTimezone);
        tvArm = findViewById(R.id.tvArm);
        videoPlayer = findViewById(R.id.spherical_video_player);
        tvLightStatus = findViewById(R.id.tvLightStatus);
        llLight = findViewById(R.id.llLight);
        llAddNew = findViewById(R.id.llAddNew);
        llSetting = findViewById(R.id.llSetting);
        instance = this;

        pd = new ProgressDialog(this);
        pd.setMessage("Connecting");
        pd.setCancelable(false);
        boolean networkAvailable = false;
        try {
            networkAvailable = NetworkInformation.Companion.isNetworkAvailable(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        P2PDev.setNetworkAvailable(networkAvailable);
        Log.e("P2p camera", "device networkAvailable " + networkAvailable);
        initListener();
        setPermissions();

    }

    private void setPermissions() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
        };
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            // ...
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.permission_required),
                    RC_CAMERA_AND_LOCATION, perms);
        }
    }


    private void initAV() {
        m_curCamera.setEnterLiveView(true);
        m_curCamera.regAVListener(MainActivity.this);
        m_curCamera.regRecvIOCtrlListener(MainActivity.this); // A2 period

        if (bSupportFishEyeLens) {
            HwH264Decoder.setHardwareDecode(true);
            videoPlayer.setVisibility(View.VISIBLE);
            m_viewLive.setVisibility(View.GONE);
            m_viewLiveHw.setVisibility(View.GONE);
//            videoPlayer.setOnTouchListener(onTouchListener);
        } else {
            videoPlayer.setVisibility(View.GONE);
            /* Setup the LiveView according to HW/SW decode. */
            if (HwH264Decoder.isUseHardwareDecoer()) {
                m_viewLive.setVisibility(View.GONE);
                m_viewLiveHw.setVisibility(View.VISIBLE);
                m_viewLiveHw.setSurfaceTextureListener(
                        new TouchedTextureView.TouchedSurfaceTextureListener(m_viewLiveHw));
                m_viewLiveHw.attachCamera(m_curCamera, 0);
            } else {
                m_viewLive.setVisibility(View.VISIBLE);
                m_viewLive.attachCamera(m_curCamera, 0);
                m_viewLiveHw.setVisibility(View.GONE);
                m_viewLiveHw.setSurfaceTextureListener(null);
            }
//            m_viewLiveHw.setOnTouchListener(onTouchListener);
//            m_viewLive.setOnTouchListener(onTouchListener);
        }

    }

    public static LiveViewHelper mLiveViewHelper = null;
    private Ex_IOCTRLGetMotionDetectResp mDetectMode;

    private void initialiseCamera(final String devId) {
        sysDID = devId;
        Log.e("P2p camera", "device ID " + sysDID);
        P2PDev camera = new P2PDev();
//            camera.set_id(model.getDeviceId());
        camera.setCam_name("4ac");
        camera.setDev_id1(devId);
        camera.setView_pwd(Utils.genPwd(this, macId));
//				camera.startConn(val * 100); // 50
        camera.setEnterLiveView(true);
        camera.setDid(devId);
        camera.assembleUID();
        camera.setWaitApConnected(true);
        if (camera.isOvSerial()) {
            camera.enableDisconnectInBackground((ActivityManager) this.getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE),
                    this.getPackageName(),
                    6);
        }
        m_curCamera = camera;
        m_curCamera.stopConn(true);
        m_curCamera.breakAllConnect();
        if (!m_curCamera.isConnected()) ;
        {
            m_curCamera.startConn(10);
        }

        m_curCamera.regAVListener(MainActivity.this);
        m_curCamera.regRecvIOCtrlListener(MainActivity.this); // A2 period

        initAV();
        mLiveViewHelper = new LiveViewHelper(MainActivity.this, m_curCamera);
        mDetectMode = new Ex_IOCTRLGetMotionDetectResp(m_curCamera);

        timerRefresh.startTimer(TIME_SLICE * 1000);

        new Thread(new Runnable() {
            @Override
            public void run() {
                initialSetup();
                initializateFunctionSetStatus();
            }
        }).start();


    }

    public static Ex_IOCTRLLightSwitch mEx_IOCTRLLightSwitch;
    public static EX_IOCTRL_PTAction mEX_IOCTRL_PTAction;
    public static Ex_IOCTRL_LIGHTING mEx_IOCTRL_LIGHTING;
    public static Ex_IOCTRL_InstantAlarm mEx_IOCTRL_InstantAlarm;

    private void initializateFunctionSetStatus() {


//        llCamera.setVisibility(m_curCamera.mParam.isSupportCaptureLiveVideo() ? View.VISIBLE : View.GONE);
//        llRecord.setVisibility(m_curCamera.mParam.isSupportRecordManually() ? View.VISIBLE : View.GONE);
//        llLight.setVisibility(m_curCamera.mParam.isSupportLightSwitch() ? View.GONE : View.GONE);
//        llIntercom.setVisibility(m_curCamera.mParam.isSupportIntercom() ? View.VISIBLE : View.GONE);
        mEx_IOCTRLLightSwitch = new Ex_IOCTRLLightSwitch(m_curCamera);
        mEX_IOCTRL_PTAction = new EX_IOCTRL_PTAction(m_curCamera);
        mEx_IOCTRL_LIGHTING = new Ex_IOCTRL_LIGHTING(m_curCamera);
        mAdvSettingHelper = new SettingAdvancedHelper(m_curCamera);


        if (m_curCamera.mParam.isSupportLightSwitch()) {
            mEx_IOCTRLLightSwitch.sendIOCtrl_fetchLightSetting();
        }


        if (m_curCamera.mParam.isSupportInstantAlarm()) {
            mEx_IOCTRL_InstantAlarm = new Ex_IOCTRL_InstantAlarm();
//            btnInstantAlarm.setVisibility(View.VISIBLE);
//            img_siren.setOnClickListener(btnInstantAlarmOnClickListener);
        } else {
            mEx_IOCTRL_InstantAlarm = null;
//            btnInstantAlarm.setVisibility(View.GONE);
        }

        Debug(DebugName.LIGHTING, "initializateFunctionSetStatus", "MainActivity", "isSupportLightOnAlways=" + m_curCamera.mParam.isSupportLightOnAlways() + " isSupportLightOnAuto=" + m_curCamera.mParam.isSupportLightOnAuto() + " isSupportLightBrightnessController=" + m_curCamera.mParam.isSupportLightBrightnessController());
        if (m_curCamera.mParam.isSupportLightOnAlways() && m_curCamera.mParam.isSupportLightOnAuto()) {
            m_curCamera.sendIOCtrl_fetchLightingSetting();
        }
    }

    private void initialSetup() {

        boolean isPortrait = DisplayInformation.isPortrait(MainActivity.this);
        DisplayMetrics ms_displayM = DisplayInformation.geDisplayMetrics(MainActivity.this);
        screenHeight = isPortrait ? ms_displayM.heightPixels : ms_displayM.widthPixels;
        screenWidth = isPortrait ? ms_displayM.widthPixels : ms_displayM.heightPixels;
        Log.d(TAG, "initialSetup(): ");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        bSupportFishEyeLens = m_curCamera.mParam.isSupportFishEyeLens()
                && Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP;
        if (m_curCamera.mParam.isRVDP()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            bSoundOn = true;
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            bSoundOn = false;
        }
        if (bSupportFishEyeLens) {
            initAVS360();
            HwH264Decoder.setHardwareDecode(true);
        } else {
            String hwDecode = ((EvoApplication) getApplication()).getPreference(EvoApplication.KEY_FUNC_LIVEVIEW_HW_DECODE_SWITCH);
            HwH264Decoder.setHardwareDecode(hwDecode.equals(EvoApplication.VALUE_FUNC_ON));
        }
        if (DEBUG) {
            Log.d(TAG, "HwH264Decoder.isUseHardwareDecoer()=" + HwH264Decoder.isUseHardwareDecoer());
        }

        if (mLiveViewHelper.isShowArmFunction(m_curCamera)) {
            initSecurityDisable();
        }
//		if(m_curCamera.mParam.getMaxVideoMode() == "720p" ) {//means GM8126 series
//		}else {
//			if (m_curCamera.mParam.isSupportSecurityDisable() || getResources().getBoolean(R.bool.support_security_disable)) {
//				initSecurityDisable();
//			}
//		}

       /* txvTitle.setText(String.format("%s(%s)", getText(R.string.app_name)
                .toString(), getText(R.string.liveview_type_realav)
                .toString()));*/
    }

    private void initSecurityDisable() {
//        Log.d(TAG, "initSecurityDisable isSupportSecurityDisable = "+m_curCamera.mParam.isSupportSecurityDisable()
//                +"\n  customized support = "+getResources().getBoolean(R.bool.support_security_disable));
        mLiveViewHelper.getArmSettingStatus();
//		m_curCamera.sendIOCtrl_fetchArmSetting();
    }

    private void initAVS360() {
        //read parameters
        InputStream inStream = getApplicationContext().getResources().openRawResource(R.raw.setting_single);

        try {
            videoPlayer.setParameter(inStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        videoPlayer.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                videoPlayer.initRenderThread(surface, width, height, handler);
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                Log.e("", "");
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                videoPlayer.releaseResources();
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            }
        });
        m_viewLiveHw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "click player", Toast.LENGTH_SHORT).show();
            }
        });
        m_viewLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "click player", Toast.LENGTH_SHORT).show();
            }
        });
        videoPlayer.setVisibility(View.VISIBLE);
        videoPlayer.setRatio(2);
    }

    private EvoApplication mainApp;

    @Override
    protected void onResume() {
        super.onResume();

//        setupViewInDifferentLayout();//onResumeTask
        determineDevice();
        mainApp = (EvoApplication) getApplication();
        String hwDecode = mainApp.getPreference(EvoApplication.KEY_FUNC_LIVEVIEW_HW_DECODE_SWITCH);
        HwH264Decoder.setHardwareDecode(hwDecode.equals(EvoApplication.VALUE_FUNC_ON));
        if (m_curCamera != null) {
            m_curCamera.setEnterLiveView(true);
            m_curCamera.regAVListener(this);
            m_curCamera.regRecvIOCtrlListener(this);
            if (m_curCamera != null && m_curCamera.mParam.isSupportPTZ_Advanced()) {
                //            btnPtzZero.setOnClickListener(btnPtzAdvancedOnClickListener);
            } else if (m_curCamera != null && m_curCamera.mParam.isSupportPTZ()) {
                //            btnPtzZero.setOnClickListener(btnPtzOnClickListener);
                //            PtAction();
            }

            timerRefresh.startTimer(TIME_SLICE * 1000);
        }
        checkWIfiConneted();
    }

    protected void exitApp(boolean callFinish) {

        if ((m_curCamera != null)) {
            m_curCamera.unregAVListener(this);
            if (callFinish) {
                m_curCamera.unregRecvIOCtrlListener(this);
            }
            m_curCamera.stopAudio();
            m_curCamera.breakAllConnect();
            m_curCamera.stopConn(true);
        }


        P2PDev.deinitAll();


    }

    private void getRVDPSwitchStatus() {
        if (m_curCamera != null) {

            byte[] req = new byte[8];
            Arrays.fill(req, (byte) 0);
            m_curCamera.sendIOCtrl_outer(
                    AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_RVDP_SWITCH_STATUS_REQ, req,
                    req.length);
        }
    }

    private void determineDevice() {
        if (m_curCamera != null) {

            if (m_curCamera.mParam.isSupportLockMode()) {
                getRVDPSwitchStatus();  //decide how many locks to be showed
            }


//			linoutArmStatus.setVisibility(View.GONE);
        }


    }

    private void checkWIfiConneted() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();
        String ssid = "";
        String[] name = info.getSSID().split("-");
        if (name.length > 1) {
            ssid = name[1];
        } else {
            ssid = name[0];
        }
        if (isAvailable) {
            if (ssid.toLowerCase().contains(macId)) {
                if (!pd.isShowing()) {
                    pd.show();
                }
                searchDiD();
            } else {
                pd.dismiss();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("").setMessage("Ooops...! Problem with " + ssid + "\n\nYou selected wrong wifi, Please select your camera from wifi" +
                        "setting and come back to the app.\n\n" +
                        "Do you want to go to wifi setting?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

//                                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            }, 200);
                        } catch (Exception e) {
                            Toast.makeText(MainActivity.this, "Something is wrong", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).show();

            }
        }
    }

    private List<SearchLAN_Result> m_listSearch = new ArrayList<SearchLAN_Result>();

    public void searchDiD() {

        LanSearchManager lanSearchManager = new LanSearchManager();
        lanSearchManager.setOnSearchListener(new LanSearchManager.OnSearchListener() {
            @Override
            public void onSearch(final LinkedList<SearchLAN_Result> result) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        updateSearchList(result);


                    }

                    private void updateSearchList(LinkedList<SearchLAN_Result> list) {
                        m_listSearch.clear();
                        String str_ssid = null, str = null;
                        for (int i = 0; i < list.size(); i++) {
                            str_ssid = list.get(i).getDevId();

                            if (getResources().getBoolean(R.bool.camera_did_filter_enable)) {
                                if (mDeviceIdentification.isSupported(str_ssid)) {
                                    m_listSearch.add(list.get(i));
                                }
                            } else {
                                m_listSearch.add(list.get(i));
                            }
                        }
                        if (m_listSearch.size() > 0) {
                            m_listSearch.get(0).getDevId();
                            initialiseCamera(m_listSearch.get(0).getDevId());
                            /*if ((m_curCamera != null)) {
                                m_curCamera.regAVListener(AddCameraDIDActivity.this);
                                m_curCamera.regRecvIOCtrlListener(AddCameraDIDActivity.this);

                                if (m_curCamera.isConnected())
                                    m_curCamera.stopConn(false);
                                m_curCamera.startConn(0);
                            }*/
                        } else {
                            searchDiD();// recursion
                            Log.e("add camera", "list search lan searchDiD again");
                        }
                    }
                });
            }
        });
        lanSearchManager.startSearch();
    }

    public void hitCheckCameraAPi() {
        Map<String, String> valueMap = new HashMap<String, String>();
        valueMap.put("macId", etMac.getText().toString().trim());
        valueMap.put("magicKey", Utils.MAGIC_KEY);
        if (Utils.isConnectedToInternet(this)) {
            pd.show();
            Call<GetSensorsModel> call = RetrofitClient.getInstance().cameraStatus(valueMap);
            call.enqueue(new Callback<GetSensorsModel>() {
                @Override
                public void onResponse(Call<GetSensorsModel> call, Response<GetSensorsModel> response) {

                    try {
                        if (response.isSuccessful()) {
                            isAvailable = true;
                            checkWIfiConneted();
                            //                        setCameraData();// add camera exisiting user
                        } else {
                            pd.dismiss();
                            APIError error = ErrorUtils.parseError(response);
                            Snackbar.make(rlContainer, error.getResponse().description(), 2000).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<GetSensorsModel> call, Throwable t) {
                    t.printStackTrace();
                    Snackbar.make(rlContainer, "Something is wrong with server", 2000).show();
                    ;
                    if (pd != null && pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            });
        } else {
            Snackbar.make(rlContainer, "Internet is not connected", 2000).show();
            ;
        }
    }


    @Override
    public void updateVFrame(Bitmap bitmap, Object o) {
        Message msg = handler.obtainMessage();
        msg.what = P2PDev.OM_SHOW_DEVICE_VIDEO;
        handler.sendMessage(msg);
    }

    @Override
    public void updateAVInfo(int codeInfo, Object o, int errCode, int intValue) {
        Message msg = handler.obtainMessage();
        msg.what = codeInfo;
        msg.arg1 = errCode;
        msg.arg2 = intValue;
        msg.obj = o;
        handler.sendMessage(msg);
    }

    @Override
    public void updateAVInfo2(int codeInfo, Object o, int errCode, int intValue) {
        Message msg = handler.obtainMessage();
        msg.what = codeInfo;
        msg.arg1 = errCode;
        msg.arg2 = intValue;
        msg.obj = o;
        handler.sendMessage(msg);
    }

    @Override
    public void onRecvIOCtrlData(int codeInfo, Object o, byte[] data) {
        Message msg = handler.obtainMessage();
        msg.what = codeInfo;
        msg.obj = o;

        if (data != null) {
            Bundle bundle = new Bundle();
            bundle.putByteArray("data", data);
            msg.setData(bundle);
        }
        handler.sendMessage(msg);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.v(TAG, "handleMessage(): msg.what=" + msg.what);
            Log.v(TAG, "handleMessage(): msg.arg2=" + msg.arg2);

            Bundle bundle = msg.getData();
            byte[] data = null;
            if (bundle != null)
                data = bundle.getByteArray("data");

            switch (msg.what) {
                case SphericalVideoPlayer.RenderThread.MSG_SURFACE_AVAILABLE:
                    m_curCamera.setSurface(videoPlayer.getVideoDecodeSurface());
                    break;
                case P2PDev.CONN_INFO_NO_NETWORK:
                    if (txvConnectStatus != null)
                        txvConnectStatus.setText(getText(R.string.tips_no_network));
                    ConnectionInfo(msg.what);
                    break;
                case P2PDev.CONN_INFO_CONNECTED: //will be triggered once after disconnecting suddenly
                    displayAV();
                    isAvailable = false;
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                    initializateFunctionSetStatus();
                    txvConnectStatus.setText(getText(R.string.info_connected));
                    ConnectionInfo(msg.what);
                    break;
                case P2PDev.CONN_INFO_CONNECTING:

                    txvConnectStatus.setText(getText(R.string.info_connecting));
                    ConnectionInfo(msg.what);
                    break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_DEVINFO_RESP:
                    if (m_curCamera.mParam.isSupportLockMode()) {
                        getRVDPSwitchStatus();  //decide how many locks to be showed
                    }
                    break;
                case P2PDev.STATUS_INFO_AV_RESOLUTION: {
                    if (msg.arg1 == 0 || msg.arg2 == 0) {
                        return;
                    }

                    if (msg.arg1 > 640 && msg.arg2 > 480) { // for HD, FHD
                        bWaitVideoDegrade = false;
                    }
                    nVWidth = msg.arg1;
                    nVHeigh = msg.arg2;
                    UpdateResolutionInfo();
                }
                break;

                case Ex_IOCTRL_LIGHTING.GET_LIGHTING_SETTING_RESP://flood light
                case Ex_IOCTRL_LIGHTING.SET_LIGHTING_SETTING_RESP:
                    if (data != null) {
                        mEx_IOCTRL_LIGHTING.setData(data);

                        final String[] optionFloodlight = {"Off", "On", "Auto"};
                        final String[] optionOutdoor = {"Off", "Auto"};
                        lightOption = (switchCam.isChecked()) ? optionOutdoor : optionFloodlight;
                        lightStatusSelected = mEx_IOCTRL_LIGHTING.getStatus();
                        if (mEx_IOCTRL_LIGHTING.getStatus() == Ex_IOCTRL_LIGHTING.STATUS_AUTO) {
                            tvLightStatus.setText("Auto");
                        } else if (mEx_IOCTRL_LIGHTING.getStatus() == Ex_IOCTRL_LIGHTING.STATUS_OFF) {

                            tvLightStatus.setText("Off");
                        } else {

                            tvLightStatus.setText("On");
                        }
                        Log.e("main", "light status " + mEx_IOCTRL_LIGHTING.getStatus());
                    }
                    break;

                case Ex_IOCTRLLightSwitch.IOCTRL_TYPE_EXTERN_GPIO_CTRL_RESP://outdoor
                    mEx_IOCTRLLightSwitch.setData(data);
                    if (data != null) {
                        bLightTurnOn = mEx_IOCTRLLightSwitch.getLightStatus() == 1;
                        if (bLightTurnOn) {
                            tvLightStatus.setText("Auto");
                            lightStatusSelected = 1;
//            btnLightSwitch.setImageResource(R.drawable.btn_lighton_liveview);
                        } else {
                            tvLightStatus.setText("OFF");
                            lightStatusSelected = 0;
//            btnLightSwitch.setImageResource(R.drawable.btn_lightoff_liveview);
                        }
                        Debug(DebugName.LIGHT_SWITCH, "EXTERN_GPIO_CTRL_RESP", "LiveViewCameraActivity" + " bLightTurnOn =" + bLightTurnOn);
                    }
                    break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_SET_TIMEZONE_RESP:
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_TIMEZONE_RESP: {
                    if (data == null)
                        break;
                    String strTimeZoneTmp = getString(data, 0);
                    String strTimeZone = formatUTC(strTimeZoneTmp);
                    break;
                }
                case P2PDev.STATUS_INFO_GET_FRAME: {
                    addFPSCount();
                }
                break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_DEVICE_CUSTOM_INFO_RESP: {// Tiger
                    if (data == null) {
                        break;
                    } else {
                        String data_string = new String(data, 48, 48).trim().replaceAll("\0", "");
//                            data_string = new String(data, 0, 128).trim();
                        Log.w("camera seting", "  firmware detail getIPCAMName! =  " + data_string);
//                        utils.setString(Constants.CAMERA_FIRMWARE_VERSION,data_string);// it will update when we go to setting through the liveview
                    }
                    break;
                }
                case P2PDev.STATUS_INFO_CAM_INDEX_FROM_DEV: {
                    /*if ((spnCameraIndex != null) &&
                            (spnCameraIndex.getSelectedItemPosition() != msg.arg2)) {
                        spnCameraIndex.setSelection(msg.arg2);
                    }*/
                }
                break;

                case P2PDev.CONN_INFO_SESSION_CLOSED: {
                    final P2PDev devTmp = (P2PDev) msg.obj;

                    txvConnectStatus.setText(getText(R.string.info_connecting));
                    if (isIntercom) {
                        if (devTmp != null) {
                            new Thread(new Runnable() {
                                public void run() {
                                    devTmp.stopIntercom(false);
                                }
                            }).start();

                        }
                        isIntercom = false;
//                        btnIntercommBig.setSelected(false);
                    }
                    ConnectionInfo(msg.what);
                }
                break;

                case P2PDev.CONN_INFO_CONNECT_WRONG_DID: {
                    txvConnectStatus.setText(getText(R.string.info_connect_wrong_did));
                    ConnectionInfo(msg.what);
                }
                break;

                case P2PDev.CONN_INFO_CONNECT_WRONG_PWD: {
                    txvConnectStatus.setText(getText(R.string.info_connect_wrong_pwd));
                    if (m_curCamera.mParam.isRVDP()) {
                        Toast.makeText(MainActivity.this, getText(R.string.info_connect_wrong_pwd), Toast.LENGTH_LONG).show();
                    }
                    ConnectionInfo(msg.what);
                }
                break;

                case P2PDev.CONN_INFO_CONNECT_FAIL:
                    txvConnectStatus.setText(getText(R.string.info_connect_fail));
                    ConnectionInfo(msg.what);
                    break;

                case P2PDev.STATUS_INFO_REMOTE_RECORDING: {
                    sdcardStatus = msg.arg2;
                   /* if (linoutRecordingNow == null)
                        break;
                    if (msg.arg2 == 0) {
                        // not recording
                        linoutRecordingNow.setVisibility(View.INVISIBLE);
                    } else if (msg.arg2 == 1) {
                        if (m_curCamera != null) {
                            if (!m_curCamera.mParam.isSupportRecordNow()) {
                                if (m_curCamera.getDeviceInfo().getTotalSDCardCapacity() > 0) {
                                    linoutRecordingNow.setVisibility(View.VISIBLE);
                                } else {
                                    linoutRecordingNow.setVisibility(View.INVISIBLE);
                                }
                            } else {
                                linoutRecordingNow.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        linoutRecordingNow.setVisibility(View.INVISIBLE);
                    }*/
                }
                break;

                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_ONET_DEVINFO_RESP: {
                    if (data == null
                            || data.length < Ex_IOCTRLGetOnetDevInfoResp.LEN_HEAD)
                        break;
                    Ex_IOCTRLGetOnetDevInfoResp stOnetresp = new Ex_IOCTRLGetOnetDevInfoResp(
                            data, 0);
                    Log.e("live", "cammera connection  = " + stOnetresp.getUser_active());
                    if (stOnetresp.getUser_active() >= 3) {
//                        quit(m_curCamera);

                       /* mDialog = new DialogNormal(MainActivity.this);
                        mDialog.setContentText(getText(R.string.tips_connection_full))
                                .setRightButton(getText(R.string.btn_ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                })
                                .setCanceledOnTouchOutside(false)
                                .show();*/
                    }
                }
                break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_VIDEO_PARAMETER_RESP:
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_SET_VIDEO_PARAMETER_RESP: {
                    if (data == null)
                        break;

                    Ex_IOCTRLGetVideoParameterResp resp = new Ex_IOCTRLGetVideoParameterResp();
                    resp.setData(data, 0);

                    int nQualIndex = m_curCamera.mParam.getSupportVideoQualitySelection((byte) resp.getQuality());
                    if (nQualIndex <= 0) {
                        Log.w("camera seting", "handleMessage(IOCTRL_TYPE_GET_VIDEO_PARAMETER_RESP): Invalid video quality!");
                        nQualIndex = 1;
                    }
                    checkVideoQuality(nQualIndex);
                    Log.w("camera seting", "  video quality! " + nQualIndex + " quality : " + resp.getQuality());

                    break;
                }

                case P2PDev.OM_SHOW_DEVICE_VIDEO: {
                    if (m_curCamera != null) {
                        if (m_curCamera.mParam.isSupportRecordManually()) {
//                            btnRec.setEnabled(true);
                        }
                    }

                    /* Setup the LiveView according to HW/SW decode. */
                    if (!HwH264Decoder.isUseHardwareDecoer()) {
                        m_viewLive.setVisibility(View.VISIBLE);
                    }
                }
                break;

                case P2PDev.OM_SHOW_OFFLINE_PIC:
                    if (!bAudioOnlyMode) {
//                        linoutOffline.setVisibility(View.VISIBLE);
                    }
                    ConnectionInfo(msg.what);
                    break;

                case P2PDev.STATUS_INFO_AV_ONLINENUM: {
                    onlineNum = msg.arg1;
                    mode = msg.arg2;
                    UpdateResolutionInfo();
                    updateSessionInfo();
                    ConnectionInfo(msg.what);
                }
                break;
                case AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_RVDP_SWITCH_STATUS_RESP: {
                }
                break;
                case Ex_IOCTRLGetMotionDetectResp.GETDETECTMODE_RESP: {//outdoor
                    if (data == null) {
                        break;
                    }

                    mDetectMode.setData(data, 0);
                    if (mDetectMode.getMotionDetectPos() == pir) {
                        setArmedStatus(true, false);//GETDETECTMODE_RESP
                    } else {

                        setArmedStatus(false, false);//GETDETECTMODE_RESP
                    }
//                    spinDetectMode.setSelection(mDetectMode.getMotionDetectPos());
//                    spinDetectMode.setEnabled(true);

//                    boolean isShow = (mDetectMode.isSoftwareMode() || mDetectMode.isSmartPIRMode()) && (m_curCamera.getParam().isSupportSwMotionDetect() || m_curCamera.getParam().isSupportSmartPirMotionDetect());
//                    linoutMotionDetectLevel.setVisibility(isShow ? View.VISIBLE : View.GONE);
//                    linoutMotionMaskBoard.setVisibility(m_curCamera.isSupportMotionMask() && isShow ? View.VISIBLE : View.GONE);


                    break;
                }
                case Ex_IOCTRLGetMotionDetectResp.SETDETECTMODE_RESP:
                    if (data == null) {
                        break;
                    }
                    mDetectMode.setData(data, 0);
                    if (mDetectMode.getMotionDetectPos() == pir) {
                        setArmedStatus(true, false);//SETDETECTMODE_RESP
                    } else {

                        setArmedStatus(false, false);//SETDETECTMODE_RESP
                    }
                    break;
                case Ex_IOCTRL_ArmSetting.SET_ARM_RESP:
                    if (data != null) {
                        mLiveViewHelper.setArmSettingStatus(data);
                        if (mLiveViewHelper.isPasswordAuthorized()) {
                            bArmed = !bArmed; //Alarm image view simulate button method, it need handler button flag.
                            setArmedStatus(bArmed, false);//SET_ARM_RESP

                           /* if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }*/
                        } else {
                            Toast.makeText(MainActivity.this, "wrong admin password", Toast.LENGTH_SHORT).show();
//                            Alert.showToast(MainActivity.this, getText(R.string.toast_wrong_admin_passwd).toString());
                        }
//					Ex_IOCTRL_ArmSetting mArmSetting = new Ex_IOCTRL_ArmSetting();
//					mArmSetting.setData(data);
//					if (mArmSetting.isAuthorized()) {
//						bArmed = !bArmed; //Alarm image view simulate button method, it need handler button flag.
//						setArmedStatus(bArmed, false);
//
//						if (mDialog != null && mDialog.isShowing()) {
//							mDialog.dismiss();
//						}
//					} else {
//						Alert.showToast(MainActivity.this, getText(R.string.toast_wrong_admin_passwd).toString());
//					}
//
                    }
                    break;
                case Ex_IOCTRL_ArmSetting.GET_ARM_RESP:
                    Log.d(TAG, "GET_ARM_RESP");

                    if (mLiveViewHelper.isShowArmFunction(m_curCamera)) {
                        Ex_IOCTRL_ArmSetting mArmSetting = new Ex_IOCTRL_ArmSetting();
                        mArmSetting.setData(data);
//                        linoutArmStatus.setVisibility(View.VISIBLE);
                        bArmed = mArmSetting.isArmEnable();
                        setArmedStatus(bArmed, false);//GET_ARM_RESP
                    }
//				if(m_curCamera.mParam.getMaxVideoMode() == "720p" ) {//means GM8126 series
//					linoutArmStatus.setVisibility(View.GONE);
//				}else {
//					if ((m_curCamera.mParam.isSupportSecurityDisable() || getResources().getBoolean(R.bool.support_security_disable))) {
////				if (m_curCamera != null && data != null && (m_curCamera.mParam.isSupportSecurityDisable() )) {
//						Ex_IOCTRL_ArmSetting mArmSetting = new Ex_IOCTRL_ArmSetting();
//						mArmSetting.setData(data);
//						linoutArmStatus.setVisibility(getResources().getBoolean(R.bool.support_security_disable) ? View.VISIBLE : View.GONE);
//						bArmed = mArmSetting.isArmEnable();
//
//						setArmedStatus(bArmed, false);
//					}
//				}
                    break;

                case Ex_IOCTRLSensorSetup.GET_RF_DEVICE_LIST_RESP:
                    if (data != null) {
                        Ex_IOCTRLSensorSetup sensorSetup = new Ex_IOCTRLSensorSetup();
                        mSensorList = sensorSetup.getStoredPositionList(data);
                        Log.d(TAG, "GET_RF_DEVICE_LIST_RESP:, data length:" + data.length + " list size:" + mSensorList.size());
                    }
                    break;

                case Ex_IOCTRL_InstantAlarm.SIREN_GET_STATUS_RESP:
                    if (data != null && mEx_IOCTRL_InstantAlarm != null) {
                        mEx_IOCTRL_InstantAlarm.setData(data);
                        if (mEx_IOCTRL_InstantAlarm.isAlarming()) {
                            m_curCamera.sendIOCtrl_InstantAlarm(mEx_IOCTRL_InstantAlarm.setStop());
                            Toast.makeText(MainActivity.this, "Siren Deactivated", Toast.LENGTH_SHORT).show();
//                            Alert.showToast(MainActivity.this, getString(R.string.txtReleaseAlarm));
                        } else {
                            m_curCamera.sendIOCtrl_InstantAlarm(mEx_IOCTRL_InstantAlarm.setStart());
                            Toast.makeText(MainActivity.this, "Siren Activated", Toast.LENGTH_SHORT).show();

//                            Alert.showToast(MainActivity.this,getString(R.string.txtInstantAlarm) );
                        }
                    }
                    break;

                case Ex_IOCTRL_InstantAlarm.EVENT_NOTIFY:
                    if (data != null && mEx_IOCTRL_InstantAlarm != null) {
                        mEx_IOCTRL_InstantAlarm.setData_BeNotified(data);
                        if (mEx_IOCTRL_InstantAlarm.isActivated()) {
                            if (mEx_IOCTRL_InstantAlarm.isAlarming()) {
//                                Alert.showToast(MainActivity.this, getString(R.string.txtInstantAlarm));
                                Toast.makeText(MainActivity.this, "Siren Activated", Toast.LENGTH_SHORT).show();
                            } else {
//                                Alert.showToast(MainActivity.this, getString(R.string.txtReleaseAlarm));
                                Toast.makeText(MainActivity.this, "Siren Deactivated", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
            super.handleMessage(msg);
        }

    };
    private String getString(byte[] data, int fromPos) { // only use for wifi and timezone
//		Log.d("ActivitySettingAdvanced", "getString");
        if (fromPos < 0)
            return "";
        StringBuilder sBuilder = new StringBuilder("");
        for (int i = fromPos; i < data.length; i++) {
            if (data[i] == 0x0)
                break;
            sBuilder.append((char) data[i]);
        }
        return sBuilder.toString();
    }

    public static String formatUTC(String sUTC) {
        StringBuffer sb = new StringBuffer("GMT");
        if (sUTC == null || sUTC.length() <= 3)
            sb.append("+00:00");
        else {
            int n1 = 0, n2 = 0;
            boolean bNega = false;
            sUTC = sUTC.toUpperCase(Locale.getDefault());
            if ((sUTC.charAt(0) == 'U' && sUTC.charAt(1) == 'T' && sUTC
                    .charAt(2) == 'C')
                    || (sUTC.charAt(0) == 'G' && sUTC.charAt(1) == 'M' && sUTC
                    .charAt(2) == 'T')) {
                sUTC = sUTC.substring(3);
                if (sUTC.charAt(0) == '-') {
                    bNega = true;
                    if (sUTC.length() == 1)
                        sUTC = "0";
                }
                if (sUTC.charAt(0) == '+') {
                    sUTC = sUTC.substring(1);
                    if (sUTC.length() == 0)
                        sUTC = "0";
                }
                String sTmp;
                String[] arr = sUTC.split(":");
                if (arr.length <= 1)
                    n1 = Integer.parseInt(sUTC);
                else {
                    n1 = Integer.parseInt(arr[0]);
                    n2 = Integer.parseInt(arr[1]);
                }
                if (n1 >= 0) {
                    if (bNega)
                        sb.append('-');
                    else
                        sb.append('+');
                } else {
                    sb.append('-');
                    n1 = -n1;
                }
                sTmp = String.format(Locale.getDefault(), "%02d", n1);
                sb.append(sTmp);
                sb.append(':');
                sTmp = String.format(Locale.getDefault(), "%02d", n2);
                sb.append(sTmp);

            } else
                sb.append("+00:00");
        }
        return sb.toString();
    }

    private void setArmedStatus(boolean armed, boolean b) {
        if (armed) {
            tvArm.setText("ARMED");
            switchArm.setChecked(true);
            switchArm.setText("Arm");
        } else {

            tvArm.setText("DISARMED");
            switchArm.setText("Disarm");
            switchArm.setChecked(false);
        }
    }

    private void updateLightDrawable() {

    }

    private int camIndex = 0; // for DWH
    private int mode = P2PDev.CONN_MODE_UNKNOWN;
    private int onlineNum = 0;
    private boolean bArmed = false;
    private boolean videoOn = true;
    private List<SensorInfo> mSensorList = null;
    private boolean bLightTurnOn = true;
    private boolean bSoundOn = false;
    public static volatile int m_LiveViewType = P2PDev.AV_TYPE_REALAV; // TODO decouple
    private long m_timeUTC = 0L;
    private String sysDID = "CGXX-001700-TMVHE";

    private void displayAV() {
        if (camIndex >= 0 && m_curCamera.isConnected()) {
            llAddNew.setVisibility(View.GONE);
            llSetting.setVisibility(View.VISIBLE);
            hideKeyboard();
//            nFrmCount = 0;
//            spnCameraIndex.setEnabled(true);

            if (P2PDev.checkSensorCam(sysDID)) {
                m_curCamera.startAV(m_LiveViewType, (byte) camIndex, m_timeUTC, videoOn, bSoundOn);
            } else {
                m_curCamera.startAV(P2PDev.AV_TYPE_REALAV, Ex_IOCTRLAVStream.QualityBySetting, (byte) camIndex,
                        m_timeUTC, mRVDP_CancelOthersAudioNotication, true);
            }

            if (m_curCamera.mParam.isSupportPTZ()) {
                m_curCamera.sendIOCtrl_fetchSensorList();
            }

            /* TMS, default switch of sounds. */
            if (!bSoundOn) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                m_curCamera.stopAudio();
            }
        }
        btnIntercom.setVisibility(m_curCamera.mParam.isSupportIntercom() ? View.VISIBLE : View.GONE);//onresume check
        if (!switchCam.isChecked()) {
            getMotionRecordig();
        } else {
            mDetectMode.fetchMotionDetect();
        }
        getVideoQuality();
        getFirmwareFilename();
        rlContainer.setVisibility(View.GONE);
    }

    private boolean bAudioOnlyMode = false; // TODO decouple
    private float i_FPS = 0.00f;
    private long nBps = 0;
    public static boolean b_adv_finish = false;
    public static boolean b_Engineering_type = false;

    private void ConnectionInfo(int AV_status) {
        String str = null, strMode = "";
        if (mode == P2PDev.CONN_MODE_P2P)
            strMode = getText(R.string.Direct_Connection).toString();// "P2P";
        else if (mode == P2PDev.CONN_MODE_RLY)
            strMode = getText(R.string.Relay_Connection).toString();// "Relay";

        if (m_curCamera != null) {
            if (m_curCamera.getConnInfo() == P2PDev.CONN_INFO_CONNECT_FAIL
                    || m_curCamera.getConnInfo() == P2PDev.CONN_INFO_SESSION_CLOSED
                    || (AV_status == P2PDev.OM_SHOW_OFFLINE_PIC && !bAudioOnlyMode)
                    || m_curCamera.getConnInfo() == P2PDev.CONN_INFO_CONNECT_WRONG_DID
                    || m_curCamera.getConnInfo() == P2PDev.CONN_INFO_CONNECT_WRONG_PWD) {
//                str = getText(R.string.Unknown).toString() + ", " + getText(R.string.Unknown).toString() + ", N=0, 0.00FPS";
//                txvSessionInfo.setText(str);// Tiger
//                btnSnapshot.setEnabled(false);
//                btnSound.setEnabled(false);
//                btnRec.setEnabled(false);
//                btnPtzZero.setEnabled(false);
//                btnLightSwitch.setEnabled(false);
//                btnIntercomm.setEnabled(false);
//                btnDoorLock1.setEnabled(false);
//                btnDoorLock2.setEnabled(false);
//                btnVideo.setEnabled(false);
//                btnIntercommBig.setEnabled(false);
//                if(btnSpeaker != null) {
//                    btnSpeaker.setEnabled(false);
//                }
//
//                linoutOffline.setVisibility(View.GONE);
//                linoutRecordingNow.setVisibility(View.INVISIBLE);
            } else {
                String sbps = humanReadableByteCount(nBps, true);
                if (i_FPS >= 0 && i_FPS <= 5) {
                    txvConnectStatus.setText(R.string.info_connecting);
                    txvConnectStatus.setTextColor(getResources().getColor(R.color.red));
                } else if (i_FPS > 5 && i_FPS <= 15) {
                    txvConnectStatus.setText(R.string.info_connected);
                    txvConnectStatus.setTextColor(getResources().getColor(R.color.green_alpha));
                } else if (i_FPS > 15) {
                    txvConnectStatus.setText(R.string.info_connected);
                    txvConnectStatus.setTextColor(getResources().getColor(R.color.green_alpha));
                }

                if (b_Engineering_type == true) {
                    str = String.format(Locale.getDefault(), "%s, %s, N=%d, %.2fFPS,  %s/s", resolutionInfo, strMode, onlineNum,
                            i_FPS, sbps);
//                    txvSessionInfo.setText(str);// Tiger
                } else {
                    String strFPS = getText(R.string.Poor).toString();
                    if (i_FPS >= 0 && i_FPS <= 5) {
                        strFPS = getText(R.string.Poor).toString();
                    } else if (i_FPS > 5 && i_FPS <= 15) {
                        strFPS = getText(R.string.Normal).toString();
                    } else if (i_FPS > 15) {
                        strFPS = getText(R.string.Good).toString();
                    }

                    str = String.format(Locale.getDefault(), "%s, %s, N=%d, %s,  %s/s", resolutionInfo, strMode, onlineNum,
                            strFPS, sbps);
                }
            }
        }
    }

    private String resolutionInfo = " - ";
    private int nVWidth = 0;
    private int nVHeigh = 0;
    private boolean bWaitVideoDegrade = false;

    private void UpdateResolutionInfo() {
        if (nVWidth > 0 && nVHeigh >= 0) {
            String str = null;
            if (nVWidth >= 1920) {
                str = "FHD";
            } else if (nVWidth >= 1280) {
                str = "HD";
            } else if (nVWidth >= 640) {
                str = "VGA";
            } else if (nVWidth >= 320) {
                str = "CIF";
            } else {
                str = "not standard size";
            }

            resolutionInfo = str;
        }
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format(Locale.US, "%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onStopTask();
        exitApp(true);
    }

    private int screenHeight = 0;
    private int screenWidth = 0;

    private void setupViewInDifferentLayout() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
        final int realHeight = displayMetrics.heightPixels;
        final int realWidth = displayMetrics.widthPixels;


        final boolean isPortrait = isPortrait();
        DisplayMetrics ms_displayM = DisplayInformation.geDisplayMetrics(MainActivity.this);
        screenHeight = isPortrait ? ms_displayM.heightPixels : ms_displayM.widthPixels;
        screenWidth = isPortrait ? ms_displayM.widthPixels : ms_displayM.heightPixels;
        //Update video layout
        if (isPortrait) {
            // PortraitLayout
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            relaoutViewParent.setBackgroundColor(0xFFFFFFFF);
        } else {
            // LandscapeLayout
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//            relaoutViewParent.setBackgroundColor(0xFF000000);
        }
        llBtn.setVisibility(isPortrait ? View.VISIBLE : View.GONE);
        etMac.setVisibility(isPortrait ? View.VISIBLE : View.GONE);
        etDid.setVisibility(isPortrait ? View.VISIBLE : View.GONE);

        //Update video frame layout
        ViewGroup.LayoutParams layParams = linoutViewArea.getLayoutParams();
        if (m_curCamera != null && m_curCamera.mParam.isSupportWideScreen()) {
//            layParams.width = isPortrait ? mViewAreaWidth : screenWidth * 16 / 9;
            layParams.width = isPortrait ? mViewAreaWidth : realWidth;
            layParams.height = isPortrait ? mViewAreaHeight : realHeight;

        } else {
            layParams.width = isPortrait ? mViewAreaWidth : realWidth;
            layParams.height = isPortrait ? mViewAreaHeight : realHeight;
        }


        linoutViewArea.setLayoutParams(layParams);
        linoutViewArea.invalidate();

        if (bSupportFishEyeLens && videoPlayer != null) {
            videoPlayer.setPreviewScreenResolution(layParams.width, layParams.height);
        }
    }


    private boolean isPortrait() {
        setDisplayMetrics();
        return DisplayInformation.isPortrait(MainActivity.this);
    }

    private int mViewAreaWidth = 0;
    private int mViewAreaHeight = 0;
    private int mRVDP_CancelOthersAudioNotication = Ex_IOCTRLAVStream.AUDIO_NOTIFY_NOT_CONSUME;
    private int nWidth;
    private int nHeight;

    private void setDisplayMetrics() {
        DisplayMetrics ms_displayM = DisplayInformation.geDisplayMetrics(MainActivity.this);
        nWidth = ms_displayM.widthPixels;
        nHeight = ms_displayM.heightPixels;
        mViewAreaWidth = ms_displayM.widthPixels;
        if (m_curCamera != null && m_curCamera.mParam.isSupportWideScreen()) {
            mViewAreaHeight = mViewAreaWidth * 9 / 16;
        } else {
            mViewAreaHeight = mViewAreaWidth * 3 / 4;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Debug(DebugName.ACTIVITY_LIFE_CYCLE, "onPause", "MainActivity");
        if (m_curCamera != null) {
            new Thread(new Runnable() {
                public void run() {
                    if (m_curCamera.isAudioSupported()) {
                        m_curCamera.stopAudio();
//                        m_curCamera.setEnterLiveView(false);
                        m_curCamera.setSound(false);
                    }
                }
            }).start();

            m_curCamera.unregRecvIOCtrlListener(this); // A2
            m_curCamera.unregAVListener(this);
        }
        lastTerminatedState = LIFE_CYCLE_ON_PAUSE;
        timerRefresh.stopTimer();
    }

    private int lastTerminatedState;
    private static final int LIFE_CYCLE_ON_PAUSE = 0;
    private static final int LIFE_CYCLE_ON_STOP = 1;

    private boolean bSupportFishEyeLens;

    private void onStopTask() {
        lastTerminatedState = LIFE_CYCLE_ON_STOP;
        if (bSupportFishEyeLens) {
        } else {
            if (HwH264Decoder.isUseHardwareDecoer()) {
                if (m_viewLiveHw != null) {
                    m_viewLiveHw.deattachCamera();
                    m_viewLiveHw.setSurfaceTextureListener(null);
                }
            } else {
                if (m_viewLive != null) {
                    m_viewLive.deattachCamera();
                }
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


        if (bSupportFishEyeLens) {
            videoPlayer.setRatio(2);
        }
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            ((AppCompatActivity) this).getSupportActionBar().show();
            Utils.showSystemUI(this);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            ((AppCompatActivity) this).getSupportActionBar().hide();
            Utils.hideSystemUI(this);
        }
        setupViewInDifferentLayout();//onConfigurationChanged
    }

    private static final int TIME_SLICE = 1; // 1s
    private static final int FPS_TIME_SPAN = 2; // 2s
    private static final int ADJUST_VIDEO_TIME_SPAN = 5; // 5s
    private static final int AUTO_ADJUST_QUALITY_COUNT = 5;
    private int nLowSpeedCount = 0;
    private int m_adjustVideoCount1s = 0;
    private int m_nTimerCount1s = 0;
    private TimerRefresh timerRefresh = new TimerRefresh(this);

    @Override
    public void onTimerUpdate() {
        updateSessionInfo();

        calculateBPS();

        m_nTimerCount1s++;
        if ((m_nTimerCount1s * TIME_SLICE) >= FPS_TIME_SPAN) {// 2s
            calculateFPS(m_nTimerCount1s * TIME_SLICE);
            m_nTimerCount1s = 0;

            if (m_curCamera.mParam.getModel() == ModelHelperSDK.DEV_MODE_WAPPESR
                    || m_curCamera.mParam.getModel() == ModelHelperSDK.DEV_MODE_WAPPES) {
                if (i_FPS < AUTO_ADJUST_QUALITY_COUNT) {
                    nLowSpeedCount++;
                } else {
                    nLowSpeedCount = 0;
                }
            }
        }

        m_adjustVideoCount1s++;
//		if ((m_adjustVideoCount1s * TIME_SLICE) >= ADJUST_VIDEO_TIME_SPAN) { //5s
        if (true) { //5s
            m_adjustVideoCount1s = 0;
            if (!bWaitVideoDegrade && m_curCamera != null && m_curCamera.isConnected() && nLowSpeedCount > 5) {
                if (nVWidth > 640 && nVHeigh > 480) {
                    nLowSpeedCount = 0;
                    bWaitVideoDegrade = true;
                    if (m_curCamera.mParam.isSupportRealMultiChannel()) ;
//                        showAdjustVideoQualityDialog();
                }
            } else {
//                if(HeadsUpAlert.isShowing()) {
//                    HeadsUpAlert.hide();}
            }
        }
    }

    private void updateSessionInfo() {
        String str = "";

        String netSpeed = humanReadableByteCount(m_curCamera.getAvgReceiveAvDataBps(), true);
        str = "Net speed:" + netSpeed;
        str += "\nFIFO video:" + m_curCamera.getFifoVideo() + " audio:" + m_curCamera.getFifoAudio();
        Log.e("updateSessionInfo", "listview " + str);
        /*if (b_Engineering_type) {
            findViewById(R.id.linoutSessionInfoSet).setVisibility(View.VISIBLE);
            String netSpeed = humanReadableByteCount(m_curCamera.getAvgReceiveAvDataBps(), true);
            str = "Net speed:" + netSpeed;
            str += "\nFIFO video:" + m_curCamera.getFifoVideo() + " audio:" + m_curCamera.getFifoAudio();
        } else {
            findViewById(R.id.linoutSessionInfoSet).setVisibility(View.INVISIBLE);
        }
        txvSessionInfo2.setText(str);*/
    }

    private volatile int nFrmCount = 0;

    public synchronized void addFPSCount() {
        nFrmCount++;
    }

    private void calculateFPS(int nElapseTimeInSec) {
        if (nElapseTimeInSec <= 0) {
            return;
        }

        int frameCount = 0;
        synchronized (this) {
            frameCount = nFrmCount;
            nFrmCount = 0;
        }

        float nFPS = 0.0f;
        if (m_curCamera != null &&
                (m_curCamera.isOvSerial() ||
                        m_curCamera.mParam.getModel() == ModelHelperSDK.DEV_MODE_WAPPESR ||
                        m_curCamera.mParam.getModel() == ModelHelperSDK.DEV_MODE_WAPPES)) {
            nFPS = (float) frameCount / (float) nElapseTimeInSec;
        } else {
            nFPS = (float) frameCount * 1.2f / (float) nElapseTimeInSec;
        }

        // Ceiling
        if (m_curCamera != null && m_curCamera.getFrameRateSetting() != 0) {
            nFPS = Math.min(nFPS, m_curCamera.getFrameRateSetting());
        }

        i_FPS = nFPS;
    }

    private long mLastReadByte = 0;

    private void calculateBPS() {
        if (m_curCamera != null) {
            long mCurReadByte = m_curCamera.getTotalReceiveAvData();
            nBps = mCurReadByte - mLastReadByte;
            mLastReadByte = mCurReadByte;
        }
    }

    static Boolean isTouched = false;

    private void initListener() {

        btConnect.setOnClickListener(this);
        llLight.setOnClickListener(this);
        btnSnap.setOnClickListener(this);
        llTimezone.setOnClickListener(this);
        btAddNewCon.setOnClickListener(this);
        btAddNew.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        llWifi.setOnClickListener(this);
        llVideoQuality.setOnClickListener(this);
        btnIntercom.setOnTouchListener(bbtnIntercomListener);
        switchArm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isTouched = true;
                return false;
            }
        });
        switchArm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isTouched) {
                    isTouched = false;
                    if (m_curCamera != null && m_curCamera.isConnected()) {
                        if (!switchCam.isChecked()) {// flood cam
                            mLiveViewHelper.setArmSetting(isChecked, Utils.genPwd(MainActivity.this, macId));
                        } else {//outdoor

                            mDetectMode.setMotionDetect(isChecked ? pir : off);
                        }
                    } else {
                        showToast(getString(R.string.camera_is_connecting));
                    }
                }
            }
        });
        switchCam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    switchCam.setText("Outdoor Cam");
                } else {
                    switchCam.setText("Flood Cam");

                }
            }
        });
        switchAudio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (m_curCamera != null) {
                    m_curCamera.setSound(isChecked);
//                m_curCamera.setAudioOutputMode();
                }
                if (isChecked) {
                    switchAudio.setText("Sound On");
                    if (m_curCamera != null) {
                        new Thread(new Runnable() {
                            public void run() {
                                m_curCamera.startAudio();
                            }
                        }).start();

                    }
                } else {
                    switchAudio.setText("Sound Off");
                    if (m_curCamera != null) {
                        new Thread(new Runnable() {
                            public void run() {
                                m_curCamera.stopAudio();
                            }
                        }).start();

                    }
                }
            }
        });
    }

    private View.OnTouchListener bbtnIntercomListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Log.e("ineercom", "action down");
                    btnIntercom.setText("Speak");
                    if (m_curCamera != null) {
//
                        new Thread(new Runnable() {

                            @Override
                            public void run() {
                                m_curCamera.startIntercom();
                            }
                        }).start(); //*
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    Log.e("ineercom", "action UP");
                    btnIntercom.setText("Hold and Speak");
                    if (m_curCamera != null) {
//                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);


                        new Thread(new Runnable() {

                            @Override
                            public void run() {
                                m_curCamera.stopIntercom(true);
                            }
                        }).start();
                    }
                    break;
            }

            return true;
        }
    };

    private int getVideoQualityModesArrayResID(int videoQualityMode) {
        switch (videoQualityMode) {
            case P2PDev.VIDEO_QUALITY_MODE_4:
            case P2PDev.VIDEO_QUALITY_MODE_2:
            case P2PDev.VIDEO_QUALITY_MODE_3:
            default:
                return R.array.video_quality;
        }
    }

    private void updateVideoQuality(int position) {
        byte[] req = new byte[8];
        Arrays.fill(req, (byte) 0);
        req[1] = 0x01;

        if (m_curCamera.mParam.isSupportVideoQuality_HD()) { // support HD Video, HD Normal, HD Better
            if (position == 0) {
                req[2] = (byte) AUTH_AV_IO_Proto.IOCTRL_QUALITY_GM_VGA_LOW;

            } else if (position == 1) {
                req[2] = (byte) AUTH_AV_IO_Proto.IOCTRL_QUALITY_HD_HIGH;
            } else {

                req[2] = (byte) AUTH_AV_IO_Proto.IOCTRL_QUALITY_GM_HD_LOW15;
            }
        } else {
            if (position == 0) {
                req[2] = (byte) AUTH_AV_IO_Proto.IOCTRL_QUALITY_QVGA_HIGH;
            } else {
                req[2] = (byte) AUTH_AV_IO_Proto.IOCTRL_QUALITY_VGA_HIGH;
            }
        }
        m_curCamera.sendIOCtrl_outer(AUTH_AV_IO_Proto.IOCTRL_TYPE_SET_VIDEO_PARAMETER_REQ, req, req.length);

        int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
        if ((position >= 1) && currentAPIVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1
                && !HwH264Decoder.isUseHardwareDecoer()) {
//            Dialogs.okDialog(getContext(), getString(R.string.hardware_decode), getString(R.string.ok), getString(R.string.open_hardware_decode), new Dialogs.okClick() {
//                @Override
//                public void ok(DialogInterface dialog) {
//                    dialog.dismiss();
//                }
//            });
        } else {
//            showApplyVideoSettings();
        }

        getVideoQuality();
//        mAdvSettingHelper.fetchVideoSetting();
    }

    String[] lightOption;
    private SettingAdvancedHelper mAdvSettingHelper = null;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btAddNewCon:
                hideKeyboard();
                isAvailable = true;
                if (etMacNew.getText().toString().length() == 0) {
                    showSnackbar(llAddNew, "Please enter Camera ID");
                    return;
                }
                macId = etMacNew.getText().toString();
                checkWIfiConneted();
//                hitCheckCameraAPi();
                break;
            case R.id.btConnect:
                hideKeyboard();
                if (etDid.getText().length() == 0 || etMac.getText().toString().length() == 0) {
                    showSnackbar(llAddNew, "Camera ID or DID is incorrect");
                    return;
                }
                macId = etMac.getText().toString();
                if (!pd.isShowing()) {
                    pd.show();
                }
                initialiseCamera(etDid.getText().toString());// existed camera
//                hitCheckCameraAPi();
                break;
            case R.id.btnSnap:
                takeSnap();
                break;
            case R.id.llWifi:
                if (!m_curCamera.isConnected()) {
                    showToast(getString(R.string.camera_is_connecting));
                    return;
                }
                Intent intent = new Intent(MainActivity.this, WifiSetupActivity.class);
                startActivity(intent);
                break;
            case R.id.btAddNew:
                llAddNew.setVisibility(View.VISIBLE);
                break;
            case R.id.btCancel:
                llAddNew.setVisibility(View.GONE);
                break;
            case R.id.llTimezone:
                final String[] timeList = getResources().getStringArray(R.array.TimeZone);
                if (!m_curCamera.isConnected()) {
                    showToast(getString(R.string.camera_is_connecting));
                    return;
                }
                AlertDialog.Builder tbuilder = new AlertDialog.Builder(MainActivity.this, R.style.MyAlertDialogStyle);
                tbuilder.setTitle(getString(R.string.time_zone));
                tbuilder.setItems(timeList, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {
                        // the user clicked on colors[which]
                        mAdvSettingHelper.updateTimeZone(timeList[pos], true);//GMT+00:00
                        mAdvSettingHelper.fetchTimeZone();

                    }
                });
                tbuilder.show();
                break;
            case R.id.llVideoQuality:
                if (!m_curCamera.isConnected()) {
                    showToast(getString(R.string.camera_is_connecting));
                    return;
                }
                final String[] vqList = getResources().getStringArray(getVideoQualityModesArrayResID(m_curCamera.mParam.getSupportVideoQualityModes()));
                AlertDialog.Builder vbuilder = new AlertDialog.Builder(MainActivity.this, R.style.MyAlertDialogStyle);
                vbuilder.setTitle(getString(R.string.video_quality));
                vbuilder.setItems(vqList, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {
                        // the user clicked on colors[which]
                        updateVideoQuality(pos);

                    }
                });
                vbuilder.show();
                break;
            case R.id.llLight:
                final String[] optionFloodlight = {"Off", "On", "Auto"};
                final String[] optionOutdoor = {"Off", "Auto"};
                if (m_curCamera == null || !m_curCamera.isConnected()) {
                    showToast(getString(R.string.camera_is_connecting));
                    return;
                }
                lightOption = (switchCam.isChecked()) ? optionOutdoor : optionFloodlight;
                ArrayAdapter<String> arrayAdapter = getArrayAdapter(lightOption);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.MyAlertDialogStyle);
                builder.setTitle(getString(R.string.night_vision));
                builder.setSingleChoiceItems(arrayAdapter, lightStatusSelected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {
                        setVision(pos);
                        tvLightStatus.setText(lightOption[pos]);
                        dialog.dismiss();
                    }
                });
                builder.show();
                break;
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static void getFirmwareFilename() {

        byte[] req0 = new byte[128];
        Arrays.fill(req0, (byte) 0);
//        byte[] bytStr1 = data_string.getBytes();
//        System.arraycopy(bytStr1, 0, req0, 0, bytStr1.length);
//        data_ip_cam_name_string = edit_ip_cam_name.getText().toString()
//                .trim();
//        byte[] bytStr2 = data_ip_cam_name_string.getBytes();
//        System.arraycopy(bytStr2, 0, req0, 16, bytStr2.length);
        m_curCamera.sendIOCtrl_outer(
                AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_DEVICE_CUSTOM_INFO_REQ,
                req0, req0.length);

    }
    public static boolean isSDCardValid() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

    // filename: such as,20101023_181010.jpg
    public static String getFileNameWithTime() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        int mSec = c.get(Calendar.SECOND);
        int mMilliSec = c.get(Calendar.MILLISECOND);

        StringBuffer sb = new StringBuffer();
        sb.append(mYear);
        if (mMonth < 10)
            sb.append('0');
        sb.append(mMonth);
        if (mDay < 10)
            sb.append('0');
        sb.append(mDay);
        sb.append('_');
        if (mHour < 10)
            sb.append('0');
        sb.append(mHour);
        if (mMinute < 10)
            sb.append('0');
        sb.append(mMinute);
        if (mSec < 10)
            sb.append('0');
        sb.append(mSec);
        sb.append("_");
        sb.append(mMilliSec);
        sb.append(".jpg");

        return sb.toString();
    }

    public static boolean saveImage(String fileName, Bitmap frame) {
        if (fileName == null || fileName.length() <= 0)
            return false;
        boolean bErr = false;
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(fileName);
            frame.compress(CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            bErr = true;

        } finally {
            if (bErr) {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }
        }
        return true;
    }


    private void takeSnap() {
        if (isSDCardValid()) {
            File dir = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/snapshot");
            if (!dir.exists()) {
                try {
                    dir.mkdir();
                } catch (SecurityException se) {
                    return;
                }
            }
            boolean bSucc = false;
            String strPicPath = dir.getAbsoluteFile().toString();
            String fileName = strPicPath + "/" + getFileNameWithTime();

            /* Capture the LiveView according to HW/SW decode. */
            Bitmap frame = null;
            if (bSupportFishEyeLens) {
                if (videoPlayer != null)
                    frame = videoPlayer.getBitmap();
            } else if (HwH264Decoder.isUseHardwareDecoer()) {
                if (m_viewLiveHw != null)
                    frame = m_viewLiveHw.getBitmap();
            } else {
                if (m_viewLive != null)
                    frame = m_viewLive.getLastFrame();
            }
            if (frame != null)
                bSucc = saveImage(fileName, frame);
            if (bSucc) {
                MediaScannerConnection
                        .scanFile(
                                MainActivity.this, // fix to Android 4.4
                                new String[]{fileName},
                                null,
                                new MediaScannerConnection.OnScanCompletedListener() {
                                    public void onScanCompleted(
                                            String path, Uri uri) {

                                    }
                                });

                showSnackbar(btnSnap, "Snapshot is successfull");
//                        Toast.makeText(LiveViewCameraActivity.this, "Snapshot is successfull", Toast.LENGTH_SHORT).show();
            } else
                showSnackbar(btnSnap, "Snapshot is failed");
//                        Toast.makeText(LiveViewCameraActivity.this, "Snapshot is failed", Toast.LENGTH_SHORT).show();
        } else {
            showSnackbar(btnSnap, "No SD Card available");
//                    Toast.makeText(LiveViewCameraActivity.this, "No SD Card available", Toast.LENGTH_SHORT).show();
        }
    }

    protected void showSnackbar(View view, String message) {
        try {
            Snackbar mSnackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
            mSnackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NotNull
    private ArrayAdapter<String> getArrayAdapter(final String[] type) {
        return new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_single_choice,
                        type) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text = (TextView) view.findViewById(android.R.id.text1);
                text.setTextColor(Color.BLACK);
                return view;
            }
        };
    }

    public void setVision(int stats) {
        // 1 for flood cam
        //0 - OFF
        //1 - ON
        //2- AUTO
//        ---------------------------------------
        // 2 oudoor camera
//        0 - OFF
//        1-AUTO
        if (!switchCam.isChecked()) {// floddcam
            int nLightingStatus = stats;
            lightStatusSelected = stats;
            if (m_curCamera != null) {
                switch (stats) {//0
                    case 0://off
                        nLightingStatus = Ex_IOCTRL_LIGHTING.STATUS_OFF;
                        break;
                    case 1://1 on
                        nLightingStatus = Ex_IOCTRL_LIGHTING.STATUS_ALWAYS_ON;
                        break;
                    case 2://2 auto
                        nLightingStatus = Ex_IOCTRL_LIGHTING.STATUS_AUTO;
                        break;
                }
                mEx_IOCTRL_LIGHTING.setStatus(nLightingStatus);
                m_curCamera.sendIOCtrl_LightingControl(mEx_IOCTRL_LIGHTING.getBytes());
                m_curCamera.sendIOCtrl_fetchLightingSetting();
            }
        } else {//OUTDOOR

           /* byte[] byt = new byte[6];
            Arrays.fill(byt, (byte) 0);
            byt[0] = (byte) (stats == 1 ? 1 : 0);
            byt[7] = (byte)1;
            outdoor_LIGHTING.setData(byt);*/
            m_curCamera.sendIOCtrl_LightControl(stats == 1 ? true : false);
            lightStatusSelected = stats;
            mEx_IOCTRLLightSwitch.sendIOCtrl_fetchLightSetting();
        }
    }

    public void showToast(String mess) {
        Toast.makeText(this, mess, Toast.LENGTH_SHORT).show();
    }

    public void getMotionRecordig() {

        byte[] req = new byte[3];
        Arrays.fill(req, (byte) 0);
        m_curCamera.sendIOCtrl_outer(Ex_IOCTRL_ArmSetting.GET_ARM_REQ, req, req.length);
    }

    public void checkVideoQuality(int a) {

        final String[] vqList = getResources().getStringArray(getVideoQualityModesArrayResID(m_curCamera.mParam.getSupportVideoQualityModes()));

        txtQual.setText(vqList[a - 1]);

    }

    public void getVideoQuality() {

        byte[] req = new byte[4];
        Arrays.fill(req, (byte) 0);
        m_curCamera.sendIOCtrl_outer(AUTH_AV_IO_Proto.IOCTRL_TYPE_GET_VIDEO_PARAMETER_REQ, req, req.length);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
